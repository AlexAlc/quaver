
using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;
using QuaverGenNHibernate.CEN.P3;



/*PROTECTED REGION ID(usingQuaverGenNHibernate.CP.P3_Cancion_anyadir_artista) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CP.P3
{
public partial class CancionCP : BasicCP
{
public void Anyadir_artista (string p_Cancion_OID, System.Collections.Generic.IList<int> p_artistas_OIDs)
{
        /*PROTECTED REGION ID(QuaverGenNHibernate.CP.P3_Cancion_anyadir_artista) ENABLED START*/

        ICancionCAD cancionCAD = null;
        CancionCEN cancionCEN = null;



        try
        {
                SessionInitializeTransaction ();
                cancionCAD = new CancionCAD (session);
                cancionCEN = new  CancionCEN (cancionCAD);






                //Call to CancionCAD

                cancionCAD.Anyadir_artista (p_Cancion_OID, p_artistas_OIDs);



                SessionCommit ();
        }
        catch (Exception ex)
        {
                SessionRollBack ();
                throw ex;
        }
        finally
        {
                SessionClose ();
        }


        /*PROTECTED REGION END*/
}
}
}
