

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;

using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


namespace QuaverGenNHibernate.CEN.P3
{
/*
 *      Definition of the class CancionCEN
 *
 */
public partial class CancionCEN
{
private ICancionCAD _ICancionCAD;

public CancionCEN()
{
        this._ICancionCAD = new CancionCAD ();
}

public CancionCEN(ICancionCAD _ICancionCAD)
{
        this._ICancionCAD = _ICancionCAD;
}

public ICancionCAD get_ICancionCAD ()
{
        return this._ICancionCAD;
}

public CancionEN Escuchar_cancion (string id)
{

        CancionEN cancionEN = null;

        cancionEN = _ICancionCAD.Escuchar_cancion (id);
        //console.log("Loaded audio '" + cancionEN.Nombre + "'");
        return cancionEN;
}



public CancionEN Modo_aleatorio (string id
                                 )
{
        CancionEN cancionEN = null;

        cancionEN = _ICancionCAD.Modo_aleatorio (id);
        return cancionEN;
}

public CancionEN Modo_repeticion (string id
                                  )
{
        CancionEN cancionEN = null;

        cancionEN = _ICancionCAD.Modo_repeticion (id);
        return cancionEN;
}

public void Borrar_cancion (string id
                            )
{
        _ICancionCAD.Borrar_cancion (id);
}

public void Anyadir_album (string p_Cancion_OID, int p_r_album_OID)
{
        //Call to CancionCAD

        _ICancionCAD.Anyadir_album (p_Cancion_OID, p_r_album_OID);
}
public string New_ (string p_nombre, string p_genero, Nullable<DateTime> p_duracion, string p_id, int p_r_album, System.Collections.Generic.IList<int> p_artistas, Nullable<DateTime> p_min_cancion, string p_letra)
{
        CancionEN cancionEN = null;
        string oid;

        //Initialized CancionEN
        cancionEN = new CancionEN ();
        cancionEN.Nombre = p_nombre;

        cancionEN.Genero = p_genero;

        cancionEN.Duracion = p_duracion;

        cancionEN.Id = p_id;


        if (p_r_album != -1) {
                // El argumento p_r_album -> Property r_album es oid = false
                // Lista de oids id
                cancionEN.R_album = new QuaverGenNHibernate.EN.P3.AlbumesEN ();
                cancionEN.R_album.Id = p_r_album;
        }


        cancionEN.Artistas = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.ArtistasEN>();
        if (p_artistas != null) {
                foreach (int item in p_artistas) {
                        QuaverGenNHibernate.EN.P3.ArtistasEN en = new QuaverGenNHibernate.EN.P3.ArtistasEN ();
                        en.Id = item;
                        cancionEN.Artistas.Add (en);
                }
        }

        else{
                cancionEN.Artistas = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.ArtistasEN>();
        }

        cancionEN.Min_cancion = p_min_cancion;

        cancionEN.Letra = p_letra;

        //Call to CancionCAD

        oid = _ICancionCAD.New_ (cancionEN);
        return oid;
}

public void Anyadir_playlist (string p_Cancion_OID, System.Collections.Generic.IList<string> p_playlist_OIDs)
{
        //Call to CancionCAD

        _ICancionCAD.Anyadir_playlist (p_Cancion_OID, p_playlist_OIDs);
}

public string New_(string p_nombre, string p_genero, Nullable<DateTime> p_duracion, AlbumesEN p_r_album, System.Collections.Generic.IList<ArtistasEN> p_artistas, Nullable<DateTime> p_min_cancion, string p_letra)
{
    CancionEN cancionEN = null;
    string oid;

    //Initialized CancionEN
    cancionEN = new CancionEN();
    cancionEN.Nombre = p_nombre;

    cancionEN.Genero = p_genero;

    cancionEN.Duracion = p_duracion;

    cancionEN.R_album = p_r_album;

    cancionEN.Artistas = p_artistas;

    cancionEN.Min_cancion = p_min_cancion;

    cancionEN.Letra = p_letra;

    oid = _ICancionCAD.New_(cancionEN);

    return oid;
}

public System.Collections.Generic.IList<CancionEN> ReadAll(int first, int size)
{
    System.Collections.Generic.IList<CancionEN> list = null;

    list = _ICancionCAD.ReadAll(first, size);
    return list;
}

public System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> ReadNombre(string p_nombre)
{
    return _ICancionCAD.ReadNombre(p_nombre);
}

public System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> ReadArtista(string p_artista)
{
    return _ICancionCAD.ReadNombre(p_artista);
}
}
}
