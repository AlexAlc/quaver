
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Usuario_hacerse_premium) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class UsuarioCEN
{
public void Hacerse_premium (string p_oid)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Usuario_hacerse_premium) ENABLED START*/
            UsuarioEN usuarioEN = null;


            usuarioEN = _IUsuarioCAD.ReadOIDDefault(p_oid);
            if (usuarioEN.Tipo_usuario.Equals(2))
            { //si no es premium
                usuarioEN.Tipo_usuario = Enumerated.P3.TipoEnum.premium;
            }


            /*PROTECTED REGION END*/
        }
    }
}
