
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Cancion_pausar_cancion) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class CancionCEN
{
public QuaverGenNHibernate.EN.P3.CancionEN Pausar_cancion (string p_Cancion_OID, int min)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Cancion_pausar_cancion) ENABLED START*/

            CancionCAD cancionCAD = new CancionCAD();
            CancionEN cancionEN = null;
            Nullable<DateTime> minuto = DateTime.Parse(min.ToString());
            cancionEN.Min_cancion = minuto;

            cancionCAD.ModifyDefault(cancionEN);

            return cancionEN;
            //throw new NotImplementedException ("Method Pausar_cancion() not yet implemented.");

            /*PROTECTED REGION END*/
        }
}
}
