

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;

using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


namespace QuaverGenNHibernate.CEN.P3
{
/*
 *      Definition of the class PlaylistCEN
 *
 */
public partial class PlaylistCEN
{
private IPlaylistCAD _IPlaylistCAD;

public PlaylistCEN()
{
        this._IPlaylistCAD = new PlaylistCAD ();
}

public PlaylistCEN(IPlaylistCAD _IPlaylistCAD)
{
        this._IPlaylistCAD = _IPlaylistCAD;
}

public IPlaylistCAD get_IPlaylistCAD ()
{
        return this._IPlaylistCAD;
}

public string Crear_playlist (string p_nombre, string p_descripcion, string p_num_canciones, Nullable<DateTime> p_fecha_creacion, string p_id, System.Collections.Generic.IList<string> p_canciones, QuaverGenNHibernate.Enumerated.P3.Tipo_playlistEnum p_tipo_playlist)
{
        PlaylistEN playlistEN = null;
        string oid;

        //Initialized PlaylistEN
        playlistEN = new PlaylistEN ();
        playlistEN.Nombre = p_nombre;

        playlistEN.Descripcion = p_descripcion;

        playlistEN.Num_canciones = p_num_canciones;

        playlistEN.Fecha_creacion = p_fecha_creacion;

        playlistEN.Id = p_id;


        playlistEN.Canciones = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.CancionEN>();
        if (p_canciones != null) {
                foreach (string item in p_canciones) {
                        QuaverGenNHibernate.EN.P3.CancionEN en = new QuaverGenNHibernate.EN.P3.CancionEN ();
                        en.Id = item;
                        playlistEN.Canciones.Add (en);
                }
        }

        else{
                playlistEN.Canciones = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.CancionEN>();
        }

        playlistEN.Tipo_playlist = p_tipo_playlist;

        //Call to PlaylistCAD

        oid = _IPlaylistCAD.Crear_playlist (playlistEN);
        return oid;
}

public void Compartir (string p_Playlist_OID, string p_usuario_OID)
{
        //Call to PlaylistCAD

        _IPlaylistCAD.Compartir (p_Playlist_OID, p_usuario_OID);
}
public void Borrar_playlist (string id
                             )
{
        _IPlaylistCAD.Borrar_playlist (id);
}

public void Editar_playlist (string p_Playlist_OID, string p_nombre, string p_descripcion, string p_num_canciones, Nullable<DateTime> p_fecha_creacion, QuaverGenNHibernate.Enumerated.P3.Tipo_playlistEnum p_tipo_playlist)
{
        PlaylistEN playlistEN = null;

        //Initialized PlaylistEN
        playlistEN = new PlaylistEN ();
        playlistEN.Id = p_Playlist_OID;
        playlistEN.Nombre = p_nombre;
        playlistEN.Descripcion = p_descripcion;
        playlistEN.Num_canciones = p_num_canciones;
        playlistEN.Fecha_creacion = p_fecha_creacion;
        playlistEN.Tipo_playlist = p_tipo_playlist;
        //Call to PlaylistCAD

        _IPlaylistCAD.Editar_playlist (playlistEN);
}

public System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> Escuchar_playlist ()
{
        return _IPlaylistCAD.Escuchar_playlist ();
}
public System.Collections.Generic.IList<PlaylistEN> ReadAll (int first, int size)
{
        System.Collections.Generic.IList<PlaylistEN> list = null;

        list = _IPlaylistCAD.ReadAll (first, size);
        return list;
}

public void Crear_playlist(string p1, string p2, DateTime? nullable, string p3, Enumerated.P3.Tipo_playlistEnum tipo_playlistEnum)
{
    throw new NotImplementedException();
}
}
}
