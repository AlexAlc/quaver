
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Radio_cambiar_emisora) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class RadioCEN
{
public void Cambiar_emisora (int p_oid, QuaverGenNHibernate.Enumerated.P3.EmisoraEnum emisora)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Radio_cambiar_emisora) ENABLED START*/

            RadioEN radioEN = null;
            RadioCAD radioCAD = new RadioCAD();

            radioEN = _IRadioCAD.ReadOIDDefault(p_oid);

            radioEN.Frecuencia = emisora;
            //para almacenar la emisora 
            radioCAD.ModifyDefault(radioEN); 
            /*PROTECTED REGION END*/
        }
}
}
