
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Administrador_iniciar_sesion) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class AdministradorCEN
{
public Boolean Iniciar_sesion (int p_oid, string contrasenya)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Administrador_iniciar_sesion) ENABLED START*/

            AdministradorEN administradorEN = null;
            bool login = false;


            administradorEN = _IAdministradorCAD.ReadOIDDefault(p_oid);

            if (contrasenya != null && administradorEN.Contrasenya == (contrasenya))
            {
                login = true;
            }


            return login;

            /*PROTECTED REGION END*/
        }
}
}
