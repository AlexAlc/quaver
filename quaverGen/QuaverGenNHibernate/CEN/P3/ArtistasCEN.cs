

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;

using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


namespace QuaverGenNHibernate.CEN.P3
{
/*
 *      Definition of the class ArtistasCEN
 *
 */
public partial class ArtistasCEN
{
private IArtistasCAD _IArtistasCAD;

public ArtistasCEN()
{
        this._IArtistasCAD = new ArtistasCAD ();
}

public ArtistasCEN(IArtistasCAD _IArtistasCAD)
{
        this._IArtistasCAD = _IArtistasCAD;
}

public IArtistasCAD get_IArtistasCAD ()
{
        return this._IArtistasCAD;
}

public int Crear_artista (string p_nombre, int p_num_seguidores, int p_id)
{
        ArtistasEN artistasEN = null;
        int oid;

        //Initialized ArtistasEN
        artistasEN = new ArtistasEN ();
        artistasEN.Nombre = p_nombre;

        artistasEN.Num_seguidores = p_num_seguidores;

        artistasEN.Id = p_id;

        //Call to ArtistasCAD

        oid = _IArtistasCAD.Crear_artista (artistasEN);
        return oid;
}

public void Borrar_artista (int id
                            )
{
        _IArtistasCAD.Borrar_artista (id);
}

public void Editar_artista (int p_Artistas_OID, string p_nombre, int p_num_seguidores)
{
        ArtistasEN artistasEN = null;

        //Initialized ArtistasEN
        artistasEN = new ArtistasEN ();
        artistasEN.Id = p_Artistas_OID;
        artistasEN.Nombre = p_nombre;
        artistasEN.Num_seguidores = p_num_seguidores;
        //Call to ArtistasCAD

        _IArtistasCAD.Editar_artista (artistasEN);
}

public System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> Escuchar_artista ()
{
        return _IArtistasCAD.Escuchar_artista ();
}

public void Crear_artista(string p1, int p2)
{
    throw new NotImplementedException();
}
}
}
