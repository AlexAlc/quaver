
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Cancion_karaoke) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class CancionCEN
{
public String Karaoke (string p_Cancion_OID, string letra)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Cancion_karaoke) ENABLED START*/

            String letrilla = null;
            CancionEN cancionEN = null;

            if (p_Cancion_OID != null && letra != null)
            {
                cancionEN = _ICancionCAD.ReadOIDDefault(p_Cancion_OID);
                letrilla = cancionEN.Letra;
            }

            return letrilla;

            /*PROTECTED REGION END*/
        }
}
}
