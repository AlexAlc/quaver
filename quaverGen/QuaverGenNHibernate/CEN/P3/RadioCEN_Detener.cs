
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Radio_detener) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class RadioCEN
{
public void Detener (int p_oid)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Radio_detener) ENABLED START*/

            RadioEN radioEN = null;
            RadioCAD radioCAD = new RadioCAD();
            //la emisora que no tiene nada es la 1, por tanto cambiamos a emisora1 para que se detenga
            radioEN.Frecuencia = Enumerated.P3.EmisoraEnum.emisora1;

            radioCAD.ModifyDefault(radioEN);

            /*PROTECTED REGION END*/
        }
}
}
