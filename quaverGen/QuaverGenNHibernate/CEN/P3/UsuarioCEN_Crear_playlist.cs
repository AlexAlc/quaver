
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Usuario_crear_playlist) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class UsuarioCEN
{
public void Crear_playlist (string p_oid, string nombre_playlist, string descripcion, Nullable<DateTime> fecha_creacion, string id_playlist, System.Collections.Generic.IList<string> p_canciones, QuaverGenNHibernate.Enumerated.P3.Tipo_playlistEnum tipo)
{
    PlaylistCAD playlistCAD = new PlaylistCAD();
    PlaylistCEN playlistCEN = new PlaylistCEN(playlistCAD);
    PlaylistEN playlistEN = new PlaylistEN();
   // IList<string> canciones = null;

    playlistEN = playlistCAD.ReadOIDDefault(p_oid);
           
   playlistCEN.Crear_playlist(nombre_playlist, descripcion , fecha_creacion, id_playlist,/* canciones,*/ tipo);



        }
    }
}
