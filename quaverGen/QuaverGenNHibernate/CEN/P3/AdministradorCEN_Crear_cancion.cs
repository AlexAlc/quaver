
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Administrador_crear_cancion) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class AdministradorCEN
{
public void Crear_cancion (int p_oid, string nombre, string id_cancion, string genero, Nullable<DateTime> duracion, int artista, int album, Nullable<DateTime> min_cancion, string letra, string nomartista, string nomalbum)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Administrador_crear_cancion) ENABLED START*/


                // Write here your custom code...
            
            CancionCAD cancionCAD = new CancionCAD();
            ArtistasCAD artistaCAD = new ArtistasCAD();
            CancionCEN cancionCEN = new CancionCEN(cancionCAD);
            ArtistasCEN artistaCEN = new ArtistasCEN(artistaCAD);
            CancionEN cancionEN = new CancionEN();
            AlbumesEN albumesEN = null;
            AlbumesCAD albumCAD = new AlbumesCAD();
            AlbumesCEN albumesCEN = new AlbumesCEN();
            ArtistasEN artistasEN = null;

            artistasEN = artistaCAD.ReadOIDDefault(artista);


            if (artistasEN == null)// no existe el artista
            {
                System.Collections.Generic.IList<int> artistas = null;
                artistas.Add(artista);
                artistaCEN.Crear_artista(nomartista, 0, artista);
                albumesEN = albumCAD.ReadOIDDefault(album);
                if (albumesEN == null)//no existe el album
                {
                    IList<string> cancion = null;
                    cancion.Add(id_cancion);
                    albumesCEN.Crear_album(nomalbum, 1, cancion, artista);
                    cancionCEN.New_(nombre, genero, duracion, id_cancion, album, artistas, min_cancion, letra);
                }
                else //existe el album
                {
                    cancionCEN.New_(nombre, genero, duracion, id_cancion, album, artistas, min_cancion, letra);
                }
            }


            else //existe el artista
            {


                System.Collections.Generic.IList<int> artistas = null;
                artistas.Add(artista);


                if (albumesEN == null)//no existe el album
                {
                    IList<string> cancion = null;
                    cancion.Add(id_cancion);
                    albumesCEN.Crear_album(nomalbum, 1, cancion, artista);
                    cancionCEN.New_(nombre, genero, duracion, id_cancion, album, artistas, min_cancion, letra);
                }
                else //existe el album
                {
                    cancionCEN.New_(nombre, genero, duracion, id_cancion, album, artistas, min_cancion, letra);
                }
            }



            // artistas = cancionCAD.ReadOIDDefault(artistaOID);




            //throw new NotImplementedException ("Method Crear_cancion() not yet implemented.");

        /*PROTECTED REGION END*/
}
}
}
