
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Usuario_cerrar_sesion) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class UsuarioCEN
{
public bool Cerrar_sesion (string p_oid)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Usuario_cerrar_sesion) ENABLED START*/

            // Write here your custom code...

            UsuarioEN usuarioEN = null;

            usuarioEN = _IUsuarioCAD.ReadOIDDefault(p_oid);

            if (this.Iniciar_sesion(p_oid,usuarioEN.Contrasenya)==true)
            {
                return true;
            }
            else {
                return false;
            }
      

            /*PROTECTED REGION END*/
        }

        /*PROTECTED REGION END*/
    }
}

