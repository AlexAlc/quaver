
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Cancion_avanzar_cancion) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class CancionCEN
{
public QuaverGenNHibernate.EN.P3.CancionEN Avanzar_cancion (string p_Cancion_OID, int min)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Cancion_avanzar_cancion) ENABLED START*/


            CancionCAD cancionCAD = new CancionCAD();
            CancionEN cancionEN = null;
            int aux;
            String dur = cancionEN.Duracion.ToString();
            aux = Int32.Parse(dur); //lo he convertido a int la duracion
            if (min < aux + 1) { //si el minuto es menor que la cancion
                 min = min + 1;
            }
            Nullable<DateTime> minuto = DateTime.Parse(min.ToString());
            
            cancionEN.Min_cancion = minuto;

            cancionCAD.ModifyDefault(cancionEN);

            return cancionEN;

           // throw new NotImplementedException ("Method Avanzar_cancion() not yet implemented.");

        /*PROTECTED REGION END*/
}
}
}
