

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;

using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


namespace QuaverGenNHibernate.CEN.P3
{
/*
 *      Definition of the class AdministradorCEN
 *
 */
public partial class AdministradorCEN
{
private IAdministradorCAD _IAdministradorCAD;

public AdministradorCEN()
{
        this._IAdministradorCAD = new AdministradorCAD ();
}

public AdministradorCEN(IAdministradorCAD _IAdministradorCAD)
{
        this._IAdministradorCAD = _IAdministradorCAD;
}

public IAdministradorCAD get_IAdministradorCAD ()
{
        return this._IAdministradorCAD;
}

public void Borrar_cuenta (int id
                           )
{
        _IAdministradorCAD.Borrar_cuenta (id);
}

public int Crear_admin (string p_nombre, String p_contrasenya)
{
        AdministradorEN administradorEN = null;
        int oid;

        //Initialized AdministradorEN
        administradorEN = new AdministradorEN ();
        administradorEN.Nombre = p_nombre;

        administradorEN.Contrasenya = Utils.Util.GetEncondeMD5 (p_contrasenya);

        //Call to AdministradorCAD

        oid = _IAdministradorCAD.Crear_admin (administradorEN);
        return oid;
}

public void Modify (int p_Administrador_OID, string p_nombre, String p_contrasenya)
{
        AdministradorEN administradorEN = null;

        //Initialized AdministradorEN
        administradorEN = new AdministradorEN ();
        administradorEN.Id = p_Administrador_OID;
        administradorEN.Nombre = p_nombre;
        administradorEN.Contrasenya = Utils.Util.GetEncondeMD5 (p_contrasenya);
        //Call to AdministradorCAD

        _IAdministradorCAD.Modify (administradorEN);
}
}
}
