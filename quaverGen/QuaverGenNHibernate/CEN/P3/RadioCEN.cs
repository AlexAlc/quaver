

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;

using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


namespace QuaverGenNHibernate.CEN.P3
{
/*
 *      Definition of the class RadioCEN
 *
 */
public partial class RadioCEN
{
private IRadioCAD _IRadioCAD;

public RadioCEN()
{
        this._IRadioCAD = new RadioCAD ();
}

public RadioCEN(IRadioCAD _IRadioCAD)
{
        this._IRadioCAD = _IRadioCAD;
}

public IRadioCAD get_IRadioCAD ()
{
        return this._IRadioCAD;
}

public int Crear_emisora (string p_nombre, QuaverGenNHibernate.Enumerated.P3.EmisoraEnum p_frecuencia)
{
        RadioEN radioEN = null;
        int oid;

        //Initialized RadioEN
        radioEN = new RadioEN ();
        radioEN.Nombre = p_nombre;

        radioEN.Frecuencia = p_frecuencia;

        //Call to RadioCAD

        oid = _IRadioCAD.Crear_emisora (radioEN);
        return oid;
}

public void Crear_emisora(Enumerated.P3.EmisoraEnum emisoraEnum, string p)
{
    throw new NotImplementedException();
}
}
}
