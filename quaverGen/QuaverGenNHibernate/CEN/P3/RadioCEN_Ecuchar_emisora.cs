
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Radio_ecuchar_emisora) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class RadioCEN
{
public void Ecuchar_emisora (int p_oid, QuaverGenNHibernate.Enumerated.P3.EmisoraEnum frecuencia)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Radio_ecuchar_emisora) ENABLED START*/

            RadioEN radioEN = null;
            RadioCAD radioCAD = new RadioCAD();

            if (frecuencia.Equals(1) == false)
            {
                radioEN.Frecuencia = frecuencia;
            }

            radioCAD.ModifyDefault(radioEN);
              /*PROTECTED REGION END*/
        }
}
}
