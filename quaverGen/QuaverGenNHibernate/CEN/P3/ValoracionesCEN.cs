

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;

using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


namespace QuaverGenNHibernate.CEN.P3
{
/*
 *      Definition of the class ValoracionesCEN
 *
 */
public partial class ValoracionesCEN
{
private IValoracionesCAD _IValoracionesCAD;

public ValoracionesCEN()
{
        this._IValoracionesCAD = new ValoracionesCAD ();
}

public ValoracionesCEN(IValoracionesCAD _IValoracionesCAD)
{
        this._IValoracionesCAD = _IValoracionesCAD;
}

public IValoracionesCAD get_IValoracionesCAD ()
{
        return this._IValoracionesCAD;
}

public int Valorar (int p_valor, string p_cancion)
{
        ValoracionesEN valoracionesEN = null;
        int oid;

        //Initialized ValoracionesEN
        valoracionesEN = new ValoracionesEN ();
        valoracionesEN.Valor = p_valor;


        if (p_cancion != null) {
                // El argumento p_cancion -> Property cancion es oid = false
                // Lista de oids id
                valoracionesEN.Cancion = new QuaverGenNHibernate.EN.P3.CancionEN ();
                valoracionesEN.Cancion.Id = p_cancion;
        }

        //Call to ValoracionesCAD

        oid = _IValoracionesCAD.Valorar (valoracionesEN);
        return oid;
}

public void Cambiar_valoracion (int p_Valoraciones_OID, int p_valor)
{
        ValoracionesEN valoracionesEN = null;

        //Initialized ValoracionesEN
        valoracionesEN = new ValoracionesEN ();
        valoracionesEN.Id = p_Valoraciones_OID;
        valoracionesEN.Valor = p_valor;
        //Call to ValoracionesCAD

        _IValoracionesCAD.Cambiar_valoracion (valoracionesEN);
}

public void Valorar(int p, Func<ICancionCAD> func)
{
    throw new NotImplementedException();
}
}
}
