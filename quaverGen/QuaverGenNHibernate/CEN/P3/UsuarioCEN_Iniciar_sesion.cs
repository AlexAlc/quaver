
using System;
using System.Text;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


/*PROTECTED REGION ID(usingQuaverGenNHibernate.CEN.P3_Usuario_iniciar_sesion) ENABLED START*/
//  references to other libraries
/*PROTECTED REGION END*/

namespace QuaverGenNHibernate.CEN.P3
{
public partial class UsuarioCEN
{
public bool Iniciar_sesion (string nameusu, String contrasenya)
{
            /*PROTECTED REGION ID(QuaverGenNHibernate.CEN.P3_Usuario_iniciar_sesion) ENABLED START*/

            UsuarioEN usuarioEN = null;
            bool login = false;
            

            usuarioEN = _IUsuarioCAD.ReadOID(nameusu);
            //string cont = Utils.Util.GetEncondeMD5(usuarioEN.Contrasenya);
           if (usuarioEN != null) { login = true; }

            if (usuarioEN!=null && usuarioEN.Contrasenya == contrasenya)
            
                login = true;
            


            return login;

            /*PROTECTED REGION END*/
        }
}
}
