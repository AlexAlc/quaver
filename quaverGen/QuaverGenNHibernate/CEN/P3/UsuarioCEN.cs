

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;

using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


namespace QuaverGenNHibernate.CEN.P3
{
/*
 *      Definition of the class UsuarioCEN
 *
 */
public partial class UsuarioCEN
{
private IUsuarioCAD _IUsuarioCAD;

public UsuarioCEN()
{
        this._IUsuarioCAD = new UsuarioCAD ();
}

public UsuarioCEN(IUsuarioCAD _IUsuarioCAD)
{
        this._IUsuarioCAD = _IUsuarioCAD;
}

public IUsuarioCAD get_IUsuarioCAD ()
{
        return this._IUsuarioCAD;
}

public void Editar_perfil (string p_Usuario_OID, string p_nombre, string p_apellidos, string p_email, String p_contrasenya, Nullable<DateTime> p_fecha_nac, QuaverGenNHibernate.Enumerated.P3.TipoEnum p_tipo_usuario)
{
        UsuarioEN usuarioEN = null;

        //Initialized UsuarioEN
        usuarioEN = new UsuarioEN ();
        usuarioEN.Nombre_usuario = p_Usuario_OID;
        usuarioEN.Nombre = p_nombre;
        usuarioEN.Apellidos = p_apellidos;
        usuarioEN.Email = p_email;
        usuarioEN.Contrasenya = Utils.Util.GetEncondeMD5 (p_contrasenya);
        usuarioEN.Fecha_nac = p_fecha_nac;
        usuarioEN.Tipo_usuario = p_tipo_usuario;
        
        //Call to UsuarioCAD

        _IUsuarioCAD.Editar_perfil (usuarioEN);
}

public string Crear_usuario(string p_nombre, string p_apellidos, string p_nombre_usuario, string p_email, String p_contrasenya, Nullable<DateTime> p_fecha_nac, QuaverGenNHibernate.Enumerated.P3.TipoEnum p_tipo_usuario)
{
        UsuarioEN usuarioEN = null;
        string oid;

        //Initialized UsuarioEN
        usuarioEN = new UsuarioEN ();
        usuarioEN.Nombre = p_nombre;

        usuarioEN.Apellidos = p_apellidos;

        usuarioEN.Nombre_usuario = p_nombre_usuario;

        usuarioEN.Email = p_email;

        usuarioEN.Contrasenya = Utils.Util.GetEncondeMD5 (p_contrasenya);

        usuarioEN.Fecha_nac = p_fecha_nac;


       /* if (p_cancion != null) {
                // El argumento p_cancion -> Property cancion es oid = false
                // Lista de oids nombre_usuario
                usuarioEN.Cancion = new QuaverGenNHibernate.EN.P3.CancionEN ();
                usuarioEN.Cancion.Id = p_cancion;
        }*/

        usuarioEN.Tipo_usuario = p_tipo_usuario;

        //Call to UsuarioCAD

        oid = _IUsuarioCAD.Crear_usuario (usuarioEN);
        return oid;
}

public void Borrar_cuenta (string nombre_usuario)
{
        _IUsuarioCAD.Borrar_cuenta (nombre_usuario);
}

public void Agregar_amigo (string p_Usuario_OID, System.Collections.Generic.IList<string> p_amigo_OIDs)
{
        //Call to UsuarioCAD

        _IUsuarioCAD.Agregar_amigo (p_Usuario_OID, p_amigo_OIDs);
}
public void Eliminar_amigo (string p_Usuario_OID, System.Collections.Generic.IList<string> p_amigo_OIDs)
{
        //Call to UsuarioCAD

        _IUsuarioCAD.Eliminar_amigo (p_Usuario_OID, p_amigo_OIDs);
}

public UsuarioEN ReadOID(string correo)
{
    UsuarioEN usuarioEN = null;

    usuarioEN = _IUsuarioCAD.ReadOID(correo);
    return usuarioEN;
}

public string Crear_usuario(string p1, string p2, string p3, string p4, string p5, DateTime nullable, string p6, Enumerated.P3.TipoEnum tipoEnum)
{
    UsuarioEN usuarioEN = null;
    string oid;
    
    //Initialized UsuarioEN
    usuarioEN = new UsuarioEN();
    usuarioEN.Nombre = p1;

    usuarioEN.Apellidos = p2;

    usuarioEN.Nombre_usuario = p3;

    usuarioEN.Email = p4;

    usuarioEN.Contrasenya = Utils.Util.GetEncondeMD5(p5);

    usuarioEN.Fecha_nac = nullable;

    usuarioEN.Tipo_usuario = tipoEnum;

    oid = _IUsuarioCAD.Crear_usuario(usuarioEN);
    return oid;
}
}
}
