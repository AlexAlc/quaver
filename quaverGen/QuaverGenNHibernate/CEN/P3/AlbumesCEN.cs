

using System;
using System.Text;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.Exceptions;

using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;


namespace QuaverGenNHibernate.CEN.P3
{
/*
 *      Definition of the class AlbumesCEN
 *
 */
public partial class AlbumesCEN
{
private IAlbumesCAD _IAlbumesCAD;

public AlbumesCEN()
{
        this._IAlbumesCAD = new AlbumesCAD ();
}

public AlbumesCEN(IAlbumesCAD _IAlbumesCAD)
{
        this._IAlbumesCAD = _IAlbumesCAD;
}

public IAlbumesCAD get_IAlbumesCAD ()
{
        return this._IAlbumesCAD;
}

public int Crear_album (string p_nombre, int p_numero_canciones, System.Collections.Generic.IList<string> p_cancion, int p_artistas)
{
        AlbumesEN albumesEN = null;
        int oid;

        //Initialized AlbumesEN
        albumesEN = new AlbumesEN ();
        albumesEN.Nombre = p_nombre;

        albumesEN.Numero_canciones = p_numero_canciones;


        albumesEN.Cancion = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.CancionEN>();
        if (p_cancion != null) {
                foreach (string item in p_cancion) {
                        QuaverGenNHibernate.EN.P3.CancionEN en = new QuaverGenNHibernate.EN.P3.CancionEN ();
                        en.Id = item;
                        albumesEN.Cancion.Add (en);
                }
        }

        else{
                albumesEN.Cancion = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.CancionEN>();
        }


        if (p_artistas != -1) {
                // El argumento p_artistas -> Property artistas es oid = false
                // Lista de oids id
                albumesEN.Artistas = new QuaverGenNHibernate.EN.P3.ArtistasEN ();
                albumesEN.Artistas.Id = p_artistas;
        }

        //Call to AlbumesCAD

        oid = _IAlbumesCAD.Crear_album (albumesEN);
        return oid;
}

public void Borrar_album (int id
                          )
{
        _IAlbumesCAD.Borrar_album (id);
}

public void Crear_album(string p1, int p2)
{
    throw new NotImplementedException();
}
}
}
