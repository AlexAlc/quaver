
using System;
using System.Text;
using QuaverGenNHibernate.CEN.P3;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.Exceptions;


/*
 * Clase Usuario:
 *
 */

namespace QuaverGenNHibernate.CAD.P3
{
public partial class UsuarioCAD : BasicCAD, IUsuarioCAD
{
public UsuarioCAD() : base ()
{
}

public UsuarioCAD(ISession sessionAux) : base (sessionAux)
{
}



public UsuarioEN ReadOIDDefault (string nombre_usuario)
{
        UsuarioEN usuarioEN = null;

        try
        {
                SessionInitializeTransaction ();
                usuarioEN = (UsuarioEN)session.Get (typeof(UsuarioEN), nombre_usuario);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in UsuarioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return usuarioEN;
}

public UsuarioEN ReadOID(string nombre_usuario)
{
    UsuarioEN usuarioEN = null;

    try
    {
        SessionInitializeTransaction();
        usuarioEN = (UsuarioEN)session.Get(typeof(UsuarioEN), nombre_usuario);
        SessionCommit();
    }

    catch (Exception ex)
    {
        SessionRollBack();
        if (ex is QuaverGenNHibernate.Exceptions.ModelException)
            throw ex;
        //throw new AllSportsGenNHibernate.Exceptions.DataLayerException ("Error in UsuarioCAD.", ex);
    }


    finally
    {
        SessionClose();
    }

    return usuarioEN;
}
public System.Collections.Generic.IList<UsuarioEN> ReadAllDefault (int first, int size)
{
        System.Collections.Generic.IList<UsuarioEN> result = null;
        try
        {
                using (ITransaction tx = session.BeginTransaction ())
                {
                        if (size > 0)
                                result = session.CreateCriteria (typeof(UsuarioEN)).
                                         SetFirstResult (first).SetMaxResults (size).List<UsuarioEN>();
                        else
                                result = session.CreateCriteria (typeof(UsuarioEN)).List<UsuarioEN>();
                }
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in UsuarioCAD.", ex);
        }

        return result;
}

// Modify default (Update all attributes of the class)

public void ModifyDefault (UsuarioEN usuario)
{
        try
        {
                SessionInitializeTransaction ();
                UsuarioEN usuarioEN = (UsuarioEN)session.Load (typeof(UsuarioEN), usuario.Nombre_usuario);

                usuarioEN.Nombre = usuario.Nombre;


                usuarioEN.Apellidos = usuario.Apellidos;


                usuarioEN.Email = usuario.Email;


                usuarioEN.Contrasenya = usuario.Contrasenya;


                usuarioEN.Fecha_nac = usuario.Fecha_nac;

                usuarioEN.Tipo_usuario = usuario.Tipo_usuario;

                session.Update (usuarioEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in UsuarioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}


public void Editar_perfil (UsuarioEN usuario)
{
        try
        {
                SessionInitializeTransaction ();
                UsuarioEN usuarioEN = (UsuarioEN)session.Load (typeof(UsuarioEN), usuario.Nombre_usuario);

                usuarioEN.Nombre = usuario.Nombre;


                usuarioEN.Apellidos = usuario.Apellidos;


                usuarioEN.Email = usuario.Email;


                usuarioEN.Contrasenya = usuario.Contrasenya;


                usuarioEN.Fecha_nac = usuario.Fecha_nac;


                usuarioEN.Tipo_usuario = usuario.Tipo_usuario;

                session.Update (usuarioEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in UsuarioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
public string Crear_usuario (UsuarioEN usuario)
{
        try
        {
                SessionInitializeTransaction ();
                if (usuario.Cancion != null) {
                        // Argumento OID y no colección.
                        usuario.Cancion = (QuaverGenNHibernate.EN.P3.CancionEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.CancionEN), usuario.Cancion.Id);

                        usuario.Cancion.Usuario
                        .Add (usuario);
                }

                session.Save (usuario);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in UsuarioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return usuario.Nombre_usuario;
}

public void Borrar_cuenta (string nombre_usuario
                           )
{
        try
        {
                SessionInitializeTransaction ();
                UsuarioEN usuarioEN = (UsuarioEN)session.Load (typeof(UsuarioEN), nombre_usuario);
                session.Delete (usuarioEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in UsuarioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public void Agregar_amigo (string p_Usuario_OID, System.Collections.Generic.IList<string> p_amigo_OIDs)
{
        QuaverGenNHibernate.EN.P3.UsuarioEN usuarioEN = null;
        try
        {
                SessionInitializeTransaction ();
                usuarioEN = (UsuarioEN)session.Load (typeof(UsuarioEN), p_Usuario_OID);
                QuaverGenNHibernate.EN.P3.UsuarioEN amigoENAux = null;
                if (usuarioEN.Amigo == null) {
                        usuarioEN.Amigo = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.UsuarioEN>();
                }

                foreach (string item in p_amigo_OIDs) {
                        amigoENAux = new QuaverGenNHibernate.EN.P3.UsuarioEN ();
                        amigoENAux = (QuaverGenNHibernate.EN.P3.UsuarioEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.UsuarioEN), item);
                        amigoENAux.Usuario.Add (usuarioEN);

                        usuarioEN.Amigo.Add (amigoENAux);
                }


                session.Update (usuarioEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in UsuarioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public void Eliminar_amigo (string p_Usuario_OID, System.Collections.Generic.IList<string> p_amigo_OIDs)
{
        try
        {
                SessionInitializeTransaction ();
                QuaverGenNHibernate.EN.P3.UsuarioEN usuarioEN = null;
                usuarioEN = (UsuarioEN)session.Load (typeof(UsuarioEN), p_Usuario_OID);

                QuaverGenNHibernate.EN.P3.UsuarioEN amigoENAux = null;
                if (usuarioEN.Amigo != null) {
                        foreach (string item in p_amigo_OIDs) {
                                amigoENAux = (QuaverGenNHibernate.EN.P3.UsuarioEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.UsuarioEN), item);
                                if (usuarioEN.Amigo.Contains (amigoENAux) == true) {
                                        usuarioEN.Amigo.Remove (amigoENAux);
                                        amigoENAux.Usuario.Remove (usuarioEN);
                                }
                                else
                                        throw new ModelException ("The identifier " + item + " in p_amigo_OIDs you are trying to unrelationer, doesn't exist in UsuarioEN");
                        }
                }

                session.Update (usuarioEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in UsuarioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
}
}
