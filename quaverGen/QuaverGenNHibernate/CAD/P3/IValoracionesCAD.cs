
using System;
using QuaverGenNHibernate.EN.P3;

namespace QuaverGenNHibernate.CAD.P3
{
public partial interface IValoracionesCAD
{
ValoracionesEN ReadOIDDefault (int id
                               );

void ModifyDefault (ValoracionesEN valoraciones);



int Valorar (ValoracionesEN valoraciones);

void Cambiar_valoracion (ValoracionesEN valoraciones);
}
}
