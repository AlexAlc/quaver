
using System;
using System.Text;
using QuaverGenNHibernate.CEN.P3;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.Exceptions;


/*
 * Clase Artistas:
 *
 */

namespace QuaverGenNHibernate.CAD.P3
{
public partial class ArtistasCAD : BasicCAD, IArtistasCAD
{
public ArtistasCAD() : base ()
{
}

public ArtistasCAD(ISession sessionAux) : base (sessionAux)
{
}



public ArtistasEN ReadOIDDefault (int id
                                  )
{
        ArtistasEN artistasEN = null;

        try
        {
                SessionInitializeTransaction ();
                artistasEN = (ArtistasEN)session.Get (typeof(ArtistasEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ArtistasCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return artistasEN;
}

public System.Collections.Generic.IList<ArtistasEN> ReadAllDefault (int first, int size)
{
        System.Collections.Generic.IList<ArtistasEN> result = null;
        try
        {
                using (ITransaction tx = session.BeginTransaction ())
                {
                        if (size > 0)
                                result = session.CreateCriteria (typeof(ArtistasEN)).
                                         SetFirstResult (first).SetMaxResults (size).List<ArtistasEN>();
                        else
                                result = session.CreateCriteria (typeof(ArtistasEN)).List<ArtistasEN>();
                }
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ArtistasCAD.", ex);
        }

        return result;
}

// Modify default (Update all attributes of the class)

public void ModifyDefault (ArtistasEN artistas)
{
        try
        {
                SessionInitializeTransaction ();
                ArtistasEN artistasEN = (ArtistasEN)session.Load (typeof(ArtistasEN), artistas.Id);

                artistasEN.Nombre = artistas.Nombre;


                artistasEN.Num_seguidores = artistas.Num_seguidores;



                session.Update (artistasEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ArtistasCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}


public int Crear_artista (ArtistasEN artistas)
{
        try
        {
                SessionInitializeTransaction ();

                session.Save (artistas);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ArtistasCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return artistas.Id;
}

public void Borrar_artista (int id
                            )
{
        try
        {
                SessionInitializeTransaction ();
                ArtistasEN artistasEN = (ArtistasEN)session.Load (typeof(ArtistasEN), id);
                session.Delete (artistasEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ArtistasCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public void Editar_artista (ArtistasEN artistas)
{
        try
        {
                SessionInitializeTransaction ();
                ArtistasEN artistasEN = (ArtistasEN)session.Load (typeof(ArtistasEN), artistas.Id);

                artistasEN.Nombre = artistas.Nombre;


                artistasEN.Num_seguidores = artistas.Num_seguidores;

                session.Update (artistasEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ArtistasCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
public System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> Escuchar_artista ()
{
        System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> result;
        try
        {
                SessionInitializeTransaction ();
                //String sql = @"FROM ArtistasEN self where FROM ArtistasEN as a, CancionEN as c where a.Nombre= c.Artista and a.Nombre isNotEmpty and c.Artista isNotEmpty;";
                //IQuery query = session.CreateQuery(sql);
                IQuery query = (IQuery)session.GetNamedQuery ("ArtistasENescuchar_artistaHQL");

                result = query.List<QuaverGenNHibernate.EN.P3.ArtistasEN>();
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ArtistasCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return result;
}
}
}
