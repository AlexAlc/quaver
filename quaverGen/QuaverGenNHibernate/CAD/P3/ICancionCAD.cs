
using System;
using QuaverGenNHibernate.EN.P3;

namespace QuaverGenNHibernate.CAD.P3
{
public partial interface ICancionCAD
{
CancionEN ReadOIDDefault (string id
                          );

void ModifyDefault (CancionEN cancion);



CancionEN Escuchar_cancion (string id
                            );





CancionEN Modo_aleatorio (string id
                          );


CancionEN Modo_repeticion (string id
                           );

System.Collections.Generic.IList<CancionEN> ReadAll(int first, int size);



void Borrar_cancion (string id
                     );


void Anyadir_artista (string p_Cancion_OID, System.Collections.Generic.IList<int> p_artistas_OIDs);

void Anyadir_album (string p_Cancion_OID, int p_r_album_OID);

string New_ (CancionEN cancion);

void Anyadir_playlist (string p_Cancion_OID, System.Collections.Generic.IList<string> p_playlist_OIDs);


System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> ReadNombre(string p_nombre);

System.Collections.Generic.IList<CancionEN> ReadArtista(System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> p_artista);

}
}
