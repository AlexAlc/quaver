
using System;
using System.Text;
using QuaverGenNHibernate.CEN.P3;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.Exceptions;


/*
 * Clase Playlist:
 *
 */

namespace QuaverGenNHibernate.CAD.P3
{
public partial class PlaylistCAD : BasicCAD, IPlaylistCAD
{
public PlaylistCAD() : base ()
{
}

public PlaylistCAD(ISession sessionAux) : base (sessionAux)
{
}



public PlaylistEN ReadOIDDefault (string id
                                  )
{
        PlaylistEN playlistEN = null;

        try
        {
                SessionInitializeTransaction ();
                playlistEN = (PlaylistEN)session.Get (typeof(PlaylistEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in PlaylistCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return playlistEN;
}

public System.Collections.Generic.IList<PlaylistEN> ReadAllDefault (int first, int size)
{
        System.Collections.Generic.IList<PlaylistEN> result = null;
        try
        {
                using (ITransaction tx = session.BeginTransaction ())
                {
                        if (size > 0)
                                result = session.CreateCriteria (typeof(PlaylistEN)).
                                         SetFirstResult (first).SetMaxResults (size).List<PlaylistEN>();
                        else
                                result = session.CreateCriteria (typeof(PlaylistEN)).List<PlaylistEN>();
                }
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in PlaylistCAD.", ex);
        }

        return result;
}

// Modify default (Update all attributes of the class)

public void ModifyDefault (PlaylistEN playlist)
{
        try
        {
                SessionInitializeTransaction ();
                PlaylistEN playlistEN = (PlaylistEN)session.Load (typeof(PlaylistEN), playlist.Id);

                playlistEN.Nombre = playlist.Nombre;


                playlistEN.Descripcion = playlist.Descripcion;


                playlistEN.Num_canciones = playlist.Num_canciones;


                playlistEN.Fecha_creacion = playlist.Fecha_creacion;



                playlistEN.Tipo_playlist = playlist.Tipo_playlist;

                session.Update (playlistEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in PlaylistCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}


public string Crear_playlist (PlaylistEN playlist)
{
        try
        {
                SessionInitializeTransaction ();
                if (playlist.Canciones != null) {
                        for (int i = 0; i < playlist.Canciones.Count; i++) {
                                playlist.Canciones [i] = (QuaverGenNHibernate.EN.P3.CancionEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.CancionEN), playlist.Canciones [i].Id);
                                playlist.Canciones [i].Playlist.Add (playlist);
                        }
                }

                session.Save (playlist);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in PlaylistCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return playlist.Id;
}

public void Compartir (string p_Playlist_OID, string p_usuario_OID)
{
        QuaverGenNHibernate.EN.P3.PlaylistEN playlistEN = null;
        try
        {
                SessionInitializeTransaction ();
                playlistEN = (PlaylistEN)session.Load (typeof(PlaylistEN), p_Playlist_OID);
              //  playlistEN.Usuario = (QuaverGenNHibernate.EN.P3.UsuarioEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.UsuarioEN), p_usuario_OID);



                session.Update (playlistEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in PlaylistCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public void Borrar_playlist (string id
                             )
{
        try
        {
                SessionInitializeTransaction ();
                PlaylistEN playlistEN = (PlaylistEN)session.Load (typeof(PlaylistEN), id);
                session.Delete (playlistEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in PlaylistCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public void Editar_playlist (PlaylistEN playlist)
{
        try
        {
                SessionInitializeTransaction ();
                PlaylistEN playlistEN = (PlaylistEN)session.Load (typeof(PlaylistEN), playlist.Id);

                playlistEN.Nombre = playlist.Nombre;


                playlistEN.Descripcion = playlist.Descripcion;


                playlistEN.Num_canciones = playlist.Num_canciones;


                playlistEN.Fecha_creacion = playlist.Fecha_creacion;


                playlistEN.Tipo_playlist = playlist.Tipo_playlist;

                session.Update (playlistEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in PlaylistCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
public System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> Escuchar_playlist ()
{
        System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> result;
        try
        {
                SessionInitializeTransaction ();
                //String sql = @"FROM PlaylistEN self where FROM PlaylistEN as p, CancionEN as c where p.Cancion=c.Id and c IsNotEmpty and p IsNotEmpty;";
                //IQuery query = session.CreateQuery(sql);
                IQuery query = (IQuery)session.GetNamedQuery ("PlaylistENescuchar_playlistHQL");

                result = query.List<QuaverGenNHibernate.EN.P3.PlaylistEN>();
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in PlaylistCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return result;
}
public System.Collections.Generic.IList<PlaylistEN> ReadAll (int first, int size)
{
        System.Collections.Generic.IList<PlaylistEN> result = null;
        try
        {
                SessionInitializeTransaction ();
                if (size > 0)
                        result = session.CreateCriteria (typeof(PlaylistEN)).
                                 SetFirstResult (first).SetMaxResults (size).List<PlaylistEN>();
                else
                        result = session.CreateCriteria (typeof(PlaylistEN)).List<PlaylistEN>();
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in PlaylistCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return result;
}
}
}
