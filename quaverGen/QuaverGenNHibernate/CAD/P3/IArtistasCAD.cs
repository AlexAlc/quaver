
using System;
using QuaverGenNHibernate.EN.P3;

namespace QuaverGenNHibernate.CAD.P3
{
public partial interface IArtistasCAD
{
ArtistasEN ReadOIDDefault (int id
                           );

void ModifyDefault (ArtistasEN artistas);



int Crear_artista (ArtistasEN artistas);

void Borrar_artista (int id
                     );


void Editar_artista (ArtistasEN artistas);


System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> Escuchar_artista ();
}
}
