
using System;
using System.Text;
using QuaverGenNHibernate.CEN.P3;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.Exceptions;


/*
 * Clase Albumes:
 *
 */

namespace QuaverGenNHibernate.CAD.P3
{
public partial class AlbumesCAD : BasicCAD, IAlbumesCAD
{
public AlbumesCAD() : base ()
{
}

public AlbumesCAD(ISession sessionAux) : base (sessionAux)
{
}



public AlbumesEN ReadOIDDefault (int id
                                 )
{
        AlbumesEN albumesEN = null;

        try
        {
                SessionInitializeTransaction ();
                albumesEN = (AlbumesEN)session.Get (typeof(AlbumesEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in AlbumesCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return albumesEN;
}

public System.Collections.Generic.IList<AlbumesEN> ReadAllDefault (int first, int size)
{
        System.Collections.Generic.IList<AlbumesEN> result = null;
        try
        {
                using (ITransaction tx = session.BeginTransaction ())
                {
                        if (size > 0)
                                result = session.CreateCriteria (typeof(AlbumesEN)).
                                         SetFirstResult (first).SetMaxResults (size).List<AlbumesEN>();
                        else
                                result = session.CreateCriteria (typeof(AlbumesEN)).List<AlbumesEN>();
                }
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in AlbumesCAD.", ex);
        }

        return result;
}

// Modify default (Update all attributes of the class)

public void ModifyDefault (AlbumesEN albumes)
{
        try
        {
                SessionInitializeTransaction ();
                AlbumesEN albumesEN = (AlbumesEN)session.Load (typeof(AlbumesEN), albumes.Id);

                albumesEN.Nombre = albumes.Nombre;


                albumesEN.Numero_canciones = albumes.Numero_canciones;



                session.Update (albumesEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in AlbumesCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}


public int Crear_album (AlbumesEN albumes)
{
        try
        {
                SessionInitializeTransaction ();
                if (albumes.Cancion != null) {
                        for (int i = 0; i < albumes.Cancion.Count; i++) {
                                albumes.Cancion [i] = (QuaverGenNHibernate.EN.P3.CancionEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.CancionEN), albumes.Cancion [i].Id);
                                albumes.Cancion [i].R_album = albumes;
                        }
                }
                if (albumes.Artistas != null) {
                        // Argumento OID y no colección.
                        albumes.Artistas = (QuaverGenNHibernate.EN.P3.ArtistasEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.ArtistasEN), albumes.Artistas.Id);

                        albumes.Artistas.Album
                        .Add (albumes);
                }

                session.Save (albumes);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in AlbumesCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return albumes.Id;
}

public void Borrar_album (int id
                          )
{
        try
        {
                SessionInitializeTransaction ();
                AlbumesEN albumesEN = (AlbumesEN)session.Load (typeof(AlbumesEN), id);
                session.Delete (albumesEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in AlbumesCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
}
}
