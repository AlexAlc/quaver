
using System;
using QuaverGenNHibernate.EN.P3;

namespace QuaverGenNHibernate.CAD.P3
{
public partial interface IPlaylistCAD
{
PlaylistEN ReadOIDDefault (string id
                           );

void ModifyDefault (PlaylistEN playlist);



string Crear_playlist (PlaylistEN playlist);

void Compartir (string p_Playlist_OID, string p_usuario_OID);

void Borrar_playlist (string id
                      );


void Editar_playlist (PlaylistEN playlist);


System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> Escuchar_playlist ();


System.Collections.Generic.IList<PlaylistEN> ReadAll (int first, int size);
}
}
