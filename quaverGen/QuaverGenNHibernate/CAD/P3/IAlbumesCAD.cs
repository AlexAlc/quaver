
using System;
using QuaverGenNHibernate.EN.P3;

namespace QuaverGenNHibernate.CAD.P3
{
public partial interface IAlbumesCAD
{
AlbumesEN ReadOIDDefault (int id
                          );

void ModifyDefault (AlbumesEN albumes);



int Crear_album (AlbumesEN albumes);

void Borrar_album (int id
                   );
}
}
