
using System;
using System.Text;
using QuaverGenNHibernate.CEN.P3;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.Exceptions;


/*
 * Clase Cancion:
 *
 */

namespace QuaverGenNHibernate.CAD.P3
{
public partial class CancionCAD : BasicCAD, ICancionCAD
{
public CancionCAD() : base ()
{
}

public CancionCAD(ISession sessionAux) : base (sessionAux)
{
}



public CancionEN ReadOIDDefault (string id
                                 )
{
        CancionEN cancionEN = null;

        try
        {
                SessionInitializeTransaction ();
                cancionEN = (CancionEN)session.Get (typeof(CancionEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return cancionEN;
}

public System.Collections.Generic.IList<CancionEN> ReadAllDefault (int first, int size)
{
        System.Collections.Generic.IList<CancionEN> result = null;
        try
        {
                using (ITransaction tx = session.BeginTransaction ())
                {
                        if (size > 0)
                                result = session.CreateCriteria (typeof(CancionEN)).
                                         SetFirstResult (first).SetMaxResults (size).List<CancionEN>();
                        else
                                result = session.CreateCriteria (typeof(CancionEN)).List<CancionEN>();
                }
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }

        return result;
}

// Modify default (Update all attributes of the class)

public void ModifyDefault (CancionEN cancion)
{
        try
        {
                SessionInitializeTransaction ();
                CancionEN cancionEN = (CancionEN)session.Load (typeof(CancionEN), cancion.Id);

                cancionEN.Nombre = cancion.Nombre;


                cancionEN.Genero = cancion.Genero;


                cancionEN.Duracion = cancion.Duracion;







                cancionEN.Min_cancion = cancion.Min_cancion;


                cancionEN.Letra = cancion.Letra;

                session.Update (cancionEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}


//Sin e: Escuchar_cancion
//Con e: CancionEN
public CancionEN Escuchar_cancion (string id
                                   )
{
        CancionEN cancionEN = null;

        try
        {
                SessionInitializeTransaction ();
                cancionEN = (CancionEN)session.Get (typeof(CancionEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return cancionEN;
}

//Sin e: Modo_aleatorio
//Con e: CancionEN
public CancionEN Modo_aleatorio (string id
                                 )
{
        CancionEN cancionEN = null;

        try
        {
                SessionInitializeTransaction ();
                cancionEN = (CancionEN)session.Get (typeof(CancionEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return cancionEN;
}

//Sin e: Modo_repeticion
//Con e: CancionEN
public CancionEN Modo_repeticion (string id
                                  )
{
        CancionEN cancionEN = null;

        try
        {
                SessionInitializeTransaction ();
                cancionEN = (CancionEN)session.Get (typeof(CancionEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return cancionEN;
}

public void Borrar_cancion (string id
                            )
{
        try
        {
                SessionInitializeTransaction ();
                CancionEN cancionEN = (CancionEN)session.Load (typeof(CancionEN), id);
                session.Delete (cancionEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public void Anyadir_artista (string p_Cancion_OID, System.Collections.Generic.IList<int> p_artistas_OIDs)
{
        QuaverGenNHibernate.EN.P3.CancionEN cancionEN = null;
        try
        {
                SessionInitializeTransaction ();
                cancionEN = (CancionEN)session.Load (typeof(CancionEN), p_Cancion_OID);
                QuaverGenNHibernate.EN.P3.ArtistasEN artistasENAux = null;
                if (cancionEN.Artistas == null) {
                        cancionEN.Artistas = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.ArtistasEN>();
                }

                foreach (int item in p_artistas_OIDs) {
                        artistasENAux = new QuaverGenNHibernate.EN.P3.ArtistasEN ();
                        artistasENAux = (QuaverGenNHibernate.EN.P3.ArtistasEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.ArtistasEN), item);
                        artistasENAux.Cancion.Add (cancionEN);

                        cancionEN.Artistas.Add (artistasENAux);
                }


                session.Update (cancionEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public void Anyadir_album (string p_Cancion_OID, int p_r_album_OID)
{
        QuaverGenNHibernate.EN.P3.CancionEN cancionEN = null;
        try
        {
                SessionInitializeTransaction ();
                cancionEN = (CancionEN)session.Load (typeof(CancionEN), p_Cancion_OID);
                cancionEN.R_album = (QuaverGenNHibernate.EN.P3.AlbumesEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.AlbumesEN), p_r_album_OID);

                cancionEN.R_album.Cancion.Add (cancionEN);



                session.Update (cancionEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}

public string New_ (CancionEN cancion)
{
        try
        {
                SessionInitializeTransaction ();
                if (cancion.R_album != null) {
                        // Argumento OID y no colección.
                        cancion.R_album = (QuaverGenNHibernate.EN.P3.AlbumesEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.AlbumesEN), cancion.R_album.Id);

                        cancion.R_album.Cancion
                        .Add (cancion);
                }
                if (cancion.Artistas != null) {
                        for (int i = 0; i < cancion.Artistas.Count; i++) {
                                cancion.Artistas [i] = (QuaverGenNHibernate.EN.P3.ArtistasEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.ArtistasEN), cancion.Artistas [i].Id);
                                cancion.Artistas [i].Cancion.Add (cancion);
                        }
                }

                session.Save (cancion);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return cancion.Id;
}

public System.Collections.Generic.IList<CancionEN> ReadAll(int first, int size)
{
    System.Collections.Generic.IList<CancionEN> result = null;
    try
    {
        SessionInitializeTransaction();
        if (size > 0)
            result = session.CreateCriteria(typeof(CancionEN)).
                     SetFirstResult(first).SetMaxResults(size).List<CancionEN>();
        else
            result = session.CreateCriteria(typeof(CancionEN)).List<CancionEN>();
        SessionCommit();
    }

    catch (Exception ex)
    {
        SessionRollBack();
        if (ex is QuaverGenNHibernate.Exceptions.ModelException)
            throw ex;
        throw new QuaverGenNHibernate.Exceptions.DataLayerException("Error in CancionCAD.", ex);
    }


    finally
    {
        SessionClose();
    }

    return result;
}

public void Anyadir_playlist (string p_Cancion_OID, System.Collections.Generic.IList<string> p_playlist_OIDs)
{
        QuaverGenNHibernate.EN.P3.CancionEN cancionEN = null;
        try
        {
                SessionInitializeTransaction ();
                cancionEN = (CancionEN)session.Load (typeof(CancionEN), p_Cancion_OID);
                QuaverGenNHibernate.EN.P3.PlaylistEN playlistENAux = null;
                if (cancionEN.Playlist == null) {
                        cancionEN.Playlist = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.PlaylistEN>();
                }

                foreach (string item in p_playlist_OIDs) {
                        playlistENAux = new QuaverGenNHibernate.EN.P3.PlaylistEN ();
                        playlistENAux = (QuaverGenNHibernate.EN.P3.PlaylistEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.PlaylistEN), item);
                        playlistENAux.Canciones.Add (cancionEN);

                        cancionEN.Playlist.Add (playlistENAux);
                }


                session.Update (cancionEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in CancionCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
     public System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> ReadNombre(string p_nombre)
        {
            System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> result;
            try
            {
                SessionInitializeTransaction();
                //String sql = @"FROM EventoEN self where FROM EventoEN where tipo=:p_preferencia";
                //IQuery query = session.CreateQuery(sql);
                IQuery query = (IQuery)session.GetNamedQuery("CancionENreadTipoHQL");
                query.SetParameter("p_preferencia", p_nombre);

                result = query.List<QuaverGenNHibernate.EN.P3.CancionEN>();
                SessionCommit();
            }

            catch (Exception ex)
            {
                SessionRollBack();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                    throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException("Error in CancionCAD.", ex);
            }


            finally
            {
                SessionClose();
            }

            return result;
        }

        public System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> ReadArtista(System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> p_artista)
        {
            System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> result;
            try
            {
                SessionInitializeTransaction();
                //String sql = @"FROM CancionEN self where FROM CancionEN where tipo=:p_artista";
                //IQuery query = session.CreateQuery(sql);
                IQuery query = (IQuery)session.GetNamedQuery("CancionENreadTipoHQL");
                query.SetParameter("p_artista", p_artista);

                result = query.List<QuaverGenNHibernate.EN.P3.CancionEN>();
                SessionCommit();
            }

            catch (Exception ex)
            {
                SessionRollBack();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                    throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException("Error in CancionCAD.", ex);
            }


            finally
            {
                SessionClose();
            }

            return result;
        }
    

}
}
