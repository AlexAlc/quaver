
using System;
using QuaverGenNHibernate.EN.P3;

namespace QuaverGenNHibernate.CAD.P3
{
public partial interface IUsuarioCAD
{
UsuarioEN ReadOIDDefault (string nombre_usuario);

void ModifyDefault (UsuarioEN usuario);


UsuarioEN ReadOID(string nombre_usuario);

void Editar_perfil (UsuarioEN usuario);


string Crear_usuario (UsuarioEN usuario);

void Borrar_cuenta (string nombre_usuario
                    );


void Agregar_amigo (string p_Usuario_OID, System.Collections.Generic.IList<string> p_amigo_OIDs);

void Eliminar_amigo (string p_Usuario_OID, System.Collections.Generic.IList<string> p_amigo_OIDs);
}
}
