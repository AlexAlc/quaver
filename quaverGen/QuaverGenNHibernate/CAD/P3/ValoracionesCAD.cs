
using System;
using System.Text;
using QuaverGenNHibernate.CEN.P3;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.Exceptions;


/*
 * Clase Valoraciones:
 *
 */

namespace QuaverGenNHibernate.CAD.P3
{
public partial class ValoracionesCAD : BasicCAD, IValoracionesCAD
{
public ValoracionesCAD() : base ()
{
}

public ValoracionesCAD(ISession sessionAux) : base (sessionAux)
{
}



public ValoracionesEN ReadOIDDefault (int id
                                      )
{
        ValoracionesEN valoracionesEN = null;

        try
        {
                SessionInitializeTransaction ();
                valoracionesEN = (ValoracionesEN)session.Get (typeof(ValoracionesEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ValoracionesCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return valoracionesEN;
}

public System.Collections.Generic.IList<ValoracionesEN> ReadAllDefault (int first, int size)
{
        System.Collections.Generic.IList<ValoracionesEN> result = null;
        try
        {
                using (ITransaction tx = session.BeginTransaction ())
                {
                        if (size > 0)
                                result = session.CreateCriteria (typeof(ValoracionesEN)).
                                         SetFirstResult (first).SetMaxResults (size).List<ValoracionesEN>();
                        else
                                result = session.CreateCriteria (typeof(ValoracionesEN)).List<ValoracionesEN>();
                }
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ValoracionesCAD.", ex);
        }

        return result;
}

// Modify default (Update all attributes of the class)

public void ModifyDefault (ValoracionesEN valoraciones)
{
        try
        {
                SessionInitializeTransaction ();
                ValoracionesEN valoracionesEN = (ValoracionesEN)session.Load (typeof(ValoracionesEN), valoraciones.Id);

                valoracionesEN.Valor = valoraciones.Valor;


                session.Update (valoracionesEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ValoracionesCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}


public int Valorar (ValoracionesEN valoraciones)
{
        try
        {
                SessionInitializeTransaction ();
                if (valoraciones.Cancion != null) {
                        // Argumento OID y no colección.
                        valoraciones.Cancion = (QuaverGenNHibernate.EN.P3.CancionEN)session.Load (typeof(QuaverGenNHibernate.EN.P3.CancionEN), valoraciones.Cancion.Id);

                        valoraciones.Cancion.Valoraciones
                                = valoraciones;
                }

                session.Save (valoraciones);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ValoracionesCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return valoraciones.Id;
}

public void Cambiar_valoracion (ValoracionesEN valoraciones)
{
        try
        {
                SessionInitializeTransaction ();
                ValoracionesEN valoracionesEN = (ValoracionesEN)session.Load (typeof(ValoracionesEN), valoraciones.Id);

                valoracionesEN.Valor = valoraciones.Valor;

                session.Update (valoracionesEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in ValoracionesCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}
}
}
