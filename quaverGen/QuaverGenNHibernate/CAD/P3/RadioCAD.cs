
using System;
using System.Text;
using QuaverGenNHibernate.CEN.P3;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;
using NHibernate.Exceptions;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.Exceptions;


/*
 * Clase Radio:
 *
 */

namespace QuaverGenNHibernate.CAD.P3
{
public partial class RadioCAD : BasicCAD, IRadioCAD
{
public RadioCAD() : base ()
{
}

public RadioCAD(ISession sessionAux) : base (sessionAux)
{
}



public RadioEN ReadOIDDefault (int id
                               )
{
        RadioEN radioEN = null;

        try
        {
                SessionInitializeTransaction ();
                radioEN = (RadioEN)session.Get (typeof(RadioEN), id);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in RadioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return radioEN;
}

public System.Collections.Generic.IList<RadioEN> ReadAllDefault (int first, int size)
{
        System.Collections.Generic.IList<RadioEN> result = null;
        try
        {
                using (ITransaction tx = session.BeginTransaction ())
                {
                        if (size > 0)
                                result = session.CreateCriteria (typeof(RadioEN)).
                                         SetFirstResult (first).SetMaxResults (size).List<RadioEN>();
                        else
                                result = session.CreateCriteria (typeof(RadioEN)).List<RadioEN>();
                }
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in RadioCAD.", ex);
        }

        return result;
}

// Modify default (Update all attributes of the class)

public void ModifyDefault (RadioEN radio)
{
        try
        {
                SessionInitializeTransaction ();
                RadioEN radioEN = (RadioEN)session.Load (typeof(RadioEN), radio.Id);

                radioEN.Nombre = radio.Nombre;


                radioEN.Frecuencia = radio.Frecuencia;


                session.Update (radioEN);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in RadioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }
}


public int Crear_emisora (RadioEN radio)
{
        try
        {
                SessionInitializeTransaction ();

                session.Save (radio);
                SessionCommit ();
        }

        catch (Exception ex) {
                SessionRollBack ();
                if (ex is QuaverGenNHibernate.Exceptions.ModelException)
                        throw ex;
                throw new QuaverGenNHibernate.Exceptions.DataLayerException ("Error in RadioCAD.", ex);
        }


        finally
        {
                SessionClose ();
        }

        return radio.Id;
}
}
}
