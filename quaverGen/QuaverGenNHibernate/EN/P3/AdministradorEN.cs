
using System;
// Definición clase AdministradorEN
namespace QuaverGenNHibernate.EN.P3
{
public partial class AdministradorEN
{
/**
 *	Atributo nombre
 */
private string nombre;



/**
 *	Atributo contrasenya
 */
private String contrasenya;



/**
 *	Atributo id
 */
private int id;



/**
 *	Atributo cancion
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> cancion;






public virtual string Nombre {
        get { return nombre; } set { nombre = value;  }
}



public virtual String Contrasenya {
        get { return contrasenya; } set { contrasenya = value;  }
}



public virtual int Id {
        get { return id; } set { id = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> Cancion {
        get { return cancion; } set { cancion = value;  }
}





public AdministradorEN()
{
        cancion = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.CancionEN>();
}



public AdministradorEN(int id, string nombre, String contrasenya, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> cancion
                       )
{
        this.init (Id, nombre, contrasenya, cancion);
}


public AdministradorEN(AdministradorEN administrador)
{
        this.init (Id, administrador.Nombre, administrador.Contrasenya, administrador.Cancion);
}

private void init (int id
                   , string nombre, String contrasenya, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> cancion)
{
        this.Id = id;


        this.Nombre = nombre;

        this.Contrasenya = contrasenya;

        this.Cancion = cancion;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        AdministradorEN t = obj as AdministradorEN;
        if (t == null)
                return false;
        if (Id.Equals (t.Id))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Id.GetHashCode ();
        return hash;
}
}
}
