
using System;
// Definición clase UsuarioEN
namespace QuaverGenNHibernate.EN.P3
{
public partial class UsuarioEN
{
/**
 *	Atributo nombre
 */
private string nombre;



/**
 *	Atributo apellidos
 */
private string apellidos;



/**
 *	Atributo nombre_usuario
 */
private string nombre_usuario;



/**
 *	Atributo email
 */
private string email;



/**
 *	Atributo contrasenya
 */
private String contrasenya;



/**
 *	Atributo fecha_nac
 */
private Nullable<DateTime> fecha_nac;



/**
 *	Atributo radio
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.RadioEN> radio;



/**
 *	Atributo playlist
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> playlist;



/**
 *	Atributo cancion
 */
private QuaverGenNHibernate.EN.P3.CancionEN cancion;



/**
 *	Atributo usuario
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> usuario;



/**
 *	Atributo amigo
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> amigo;



/**
 *	Atributo tipo_usuario
 */
private QuaverGenNHibernate.Enumerated.P3.TipoEnum tipo_usuario;






public virtual string Nombre {
        get { return nombre; } set { nombre = value;  }
}



public virtual string Apellidos {
        get { return apellidos; } set { apellidos = value;  }
}



public virtual string Nombre_usuario {
        get { return nombre_usuario; } set { nombre_usuario = value;  }
}



public virtual string Email {
        get { return email; } set { email = value;  }
}



public virtual String Contrasenya {
        get { return contrasenya; } set { contrasenya = value;  }
}



public virtual Nullable<DateTime> Fecha_nac {
        get { return fecha_nac; } set { fecha_nac = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.RadioEN> Radio {
        get { return radio; } set { radio = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> Playlist {
        get { return playlist; } set { playlist = value;  }
}



public virtual QuaverGenNHibernate.EN.P3.CancionEN Cancion {
        get { return cancion; } set { cancion = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> Usuario {
        get { return usuario; } set { usuario = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> Amigo {
        get { return amigo; } set { amigo = value;  }
}



public virtual QuaverGenNHibernate.Enumerated.P3.TipoEnum Tipo_usuario {
        get { return tipo_usuario; } set { tipo_usuario = value;  }
}





public UsuarioEN()
{
        radio = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.RadioEN>();
        playlist = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.PlaylistEN>();
        usuario = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.UsuarioEN>();
        amigo = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.UsuarioEN>();
}



public UsuarioEN(string nombre_usuario, string nombre, string apellidos, string email, String contrasenya, Nullable<DateTime> fecha_nac, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.RadioEN> radio, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> playlist, QuaverGenNHibernate.EN.P3.CancionEN cancion, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> usuario, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> amigo, QuaverGenNHibernate.Enumerated.P3.TipoEnum tipo_usuario
                 )
{
        this.init (Nombre_usuario, nombre, apellidos, email, contrasenya, fecha_nac, radio, playlist, cancion, usuario, amigo, tipo_usuario);
}


public UsuarioEN(UsuarioEN usuario)
{
        this.init (Nombre_usuario, usuario.Nombre, usuario.Apellidos, usuario.Email, usuario.Contrasenya, usuario.Fecha_nac, usuario.Radio, usuario.Playlist, usuario.Cancion, usuario.Usuario, usuario.Amigo, usuario.Tipo_usuario);
}

private void init (string nombre_usuario
                   , string nombre, string apellidos, string email, String contrasenya, Nullable<DateTime> fecha_nac, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.RadioEN> radio, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> playlist, QuaverGenNHibernate.EN.P3.CancionEN cancion, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> usuario, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> amigo, QuaverGenNHibernate.Enumerated.P3.TipoEnum tipo_usuario)
{
        this.Nombre_usuario = nombre_usuario;


        this.Nombre = nombre;

        this.Apellidos = apellidos;

        this.Email = email;

        this.Contrasenya = contrasenya;

        this.Fecha_nac = fecha_nac;

        this.Radio = radio;

        this.Playlist = playlist;

        this.Cancion = cancion;

        this.Usuario = usuario;

        this.Amigo = amigo;

        this.Tipo_usuario = tipo_usuario;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        UsuarioEN t = obj as UsuarioEN;
        if (t == null)
                return false;
        if (Nombre_usuario.Equals (t.Nombre_usuario))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Nombre_usuario.GetHashCode ();
        return hash;
}
}
}
