
using System;
// Definición clase RadioEN
namespace QuaverGenNHibernate.EN.P3
{
public partial class RadioEN
{
/**
 *	Atributo nombre
 */
private string nombre;



/**
 *	Atributo frecuencia
 */
private QuaverGenNHibernate.Enumerated.P3.EmisoraEnum frecuencia;



/**
 *	Atributo id
 */
private int id;



/**
 *	Atributo usuario
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> usuario;






public virtual string Nombre {
        get { return nombre; } set { nombre = value;  }
}



public virtual QuaverGenNHibernate.Enumerated.P3.EmisoraEnum Frecuencia {
        get { return frecuencia; } set { frecuencia = value;  }
}



public virtual int Id {
        get { return id; } set { id = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> Usuario {
        get { return usuario; } set { usuario = value;  }
}





public RadioEN()
{
        usuario = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.UsuarioEN>();
}



public RadioEN(int id, string nombre, QuaverGenNHibernate.Enumerated.P3.EmisoraEnum frecuencia, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> usuario
               )
{
        this.init (Id, nombre, frecuencia, usuario);
}


public RadioEN(RadioEN radio)
{
        this.init (Id, radio.Nombre, radio.Frecuencia, radio.Usuario);
}

private void init (int id
                   , string nombre, QuaverGenNHibernate.Enumerated.P3.EmisoraEnum frecuencia, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> usuario)
{
        this.Id = id;


        this.Nombre = nombre;

        this.Frecuencia = frecuencia;

        this.Usuario = usuario;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        RadioEN t = obj as RadioEN;
        if (t == null)
                return false;
        if (Id.Equals (t.Id))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Id.GetHashCode ();
        return hash;
}
}
}
