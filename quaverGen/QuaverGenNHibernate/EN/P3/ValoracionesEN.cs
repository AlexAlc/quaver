
using System;
// Definición clase ValoracionesEN
namespace QuaverGenNHibernate.EN.P3
{
public partial class ValoracionesEN
{
/**
 *	Atributo valor
 */
private int valor;



/**
 *	Atributo id
 */
private int id;



/**
 *	Atributo cancion
 */
private QuaverGenNHibernate.EN.P3.CancionEN cancion;






public virtual int Valor {
        get { return valor; } set { valor = value;  }
}



public virtual int Id {
        get { return id; } set { id = value;  }
}



public virtual QuaverGenNHibernate.EN.P3.CancionEN Cancion {
        get { return cancion; } set { cancion = value;  }
}





public ValoracionesEN()
{
}



public ValoracionesEN(int id, int valor, QuaverGenNHibernate.EN.P3.CancionEN cancion
                      )
{
        this.init (Id, valor, cancion);
}


public ValoracionesEN(ValoracionesEN valoraciones)
{
        this.init (Id, valoraciones.Valor, valoraciones.Cancion);
}

private void init (int id
                   , int valor, QuaverGenNHibernate.EN.P3.CancionEN cancion)
{
        this.Id = id;


        this.Valor = valor;

        this.Cancion = cancion;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        ValoracionesEN t = obj as ValoracionesEN;
        if (t == null)
                return false;
        if (Id.Equals (t.Id))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Id.GetHashCode ();
        return hash;
}
}
}
