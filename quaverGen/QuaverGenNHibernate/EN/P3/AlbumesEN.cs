
using System;
// Definición clase AlbumesEN
namespace QuaverGenNHibernate.EN.P3
{
public partial class AlbumesEN
{
/**
 *	Atributo nombre
 */
private string nombre;



/**
 *	Atributo numero_canciones
 */
private int numero_canciones;



/**
 *	Atributo id
 */
private int id;



/**
 *	Atributo cancion
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> cancion;



/**
 *	Atributo artistas
 */
private QuaverGenNHibernate.EN.P3.ArtistasEN artistas;






public virtual string Nombre {
        get { return nombre; } set { nombre = value;  }
}



public virtual int Numero_canciones {
        get { return numero_canciones; } set { numero_canciones = value;  }
}



public virtual int Id {
        get { return id; } set { id = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> Cancion {
        get { return cancion; } set { cancion = value;  }
}



public virtual QuaverGenNHibernate.EN.P3.ArtistasEN Artistas {
        get { return artistas; } set { artistas = value;  }
}





public AlbumesEN()
{
        cancion = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.CancionEN>();
}



public AlbumesEN(int id, string nombre, int numero_canciones, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> cancion, QuaverGenNHibernate.EN.P3.ArtistasEN artistas
                 )
{
        this.init (Id, nombre, numero_canciones, cancion, artistas);
}


public AlbumesEN(AlbumesEN albumes)
{
        this.init (Id, albumes.Nombre, albumes.Numero_canciones, albumes.Cancion, albumes.Artistas);
}

private void init (int id
                   , string nombre, int numero_canciones, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> cancion, QuaverGenNHibernate.EN.P3.ArtistasEN artistas)
{
        this.Id = id;


        this.Nombre = nombre;

        this.Numero_canciones = numero_canciones;

        this.Cancion = cancion;

        this.Artistas = artistas;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        AlbumesEN t = obj as AlbumesEN;
        if (t == null)
                return false;
        if (Id.Equals (t.Id))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Id.GetHashCode ();
        return hash;
}
}
}
