
using System;
// Definición clase CancionEN
namespace QuaverGenNHibernate.EN.P3
{
public partial class CancionEN
{
/**
 *	Atributo nombre
 */
private string nombre;



/**
 *	Atributo genero
 */
private string genero;



/**
 *	Atributo duracion
 */
private Nullable<DateTime> duracion;



/**
 *	Atributo id
 */
private string id;



/**
 *	Atributo usuario
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> usuario;



/**
 *	Atributo r_album
 */
private QuaverGenNHibernate.EN.P3.AlbumesEN r_album;



/**
 *	Atributo artistas
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> artistas;



/**
 *	Atributo valoraciones
 */
private QuaverGenNHibernate.EN.P3.ValoracionesEN valoraciones;



/**
 *	Atributo playlist
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> playlist;



/**
 *	Atributo min_cancion
 */
private Nullable<DateTime> min_cancion;



/**
 *	Atributo letra
 */
private string letra;






public virtual string Nombre {
        get { return nombre; } set { nombre = value;  }
}



public virtual string Genero {
        get { return genero; } set { genero = value;  }
}



public virtual Nullable<DateTime> Duracion {
        get { return duracion; } set { duracion = value;  }
}



public virtual string Id {
        get { return id; } set { id = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> Usuario {
        get { return usuario; } set { usuario = value;  }
}



public virtual QuaverGenNHibernate.EN.P3.AlbumesEN R_album {
        get { return r_album; } set { r_album = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> Artistas {
        get { return artistas; } set { artistas = value;  }
}



public virtual QuaverGenNHibernate.EN.P3.ValoracionesEN Valoraciones {
        get { return valoraciones; } set { valoraciones = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> Playlist {
        get { return playlist; } set { playlist = value;  }
}



public virtual Nullable<DateTime> Min_cancion {
        get { return min_cancion; } set { min_cancion = value;  }
}



public virtual string Letra {
        get { return letra; } set { letra = value;  }
}





public CancionEN()
{
        usuario = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.UsuarioEN>();
        artistas = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.ArtistasEN>();
        playlist = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.PlaylistEN>();
}



public CancionEN(string id, string nombre, string genero, Nullable<DateTime> duracion, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> usuario, QuaverGenNHibernate.EN.P3.AlbumesEN r_album, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> artistas, QuaverGenNHibernate.EN.P3.ValoracionesEN valoraciones, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> playlist, Nullable<DateTime> min_cancion, string letra
                 )
{
        this.init (Id, nombre, genero, duracion, usuario, r_album, artistas, valoraciones, playlist, min_cancion, letra);
}


public CancionEN(CancionEN cancion)
{
        this.init (Id, cancion.Nombre, cancion.Genero, cancion.Duracion, cancion.Usuario, cancion.R_album, cancion.Artistas, cancion.Valoraciones, cancion.Playlist, cancion.Min_cancion, cancion.Letra);
}

private void init (string id
                   , string nombre, string genero, Nullable<DateTime> duracion, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.UsuarioEN> usuario, QuaverGenNHibernate.EN.P3.AlbumesEN r_album, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.ArtistasEN> artistas, QuaverGenNHibernate.EN.P3.ValoracionesEN valoraciones, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.PlaylistEN> playlist, Nullable<DateTime> min_cancion, string letra)
{
        this.Id = id;


        this.Nombre = nombre;

        this.Genero = genero;

        this.Duracion = duracion;

        this.Usuario = usuario;

        this.R_album = r_album;

        this.Artistas = artistas;

        this.Valoraciones = valoraciones;

        this.Playlist = playlist;

        this.Min_cancion = min_cancion;

        this.Letra = letra;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        CancionEN t = obj as CancionEN;
        if (t == null)
                return false;
        if (Id.Equals (t.Id))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Id.GetHashCode ();
        return hash;
}
}
}
