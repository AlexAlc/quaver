
using System;
// Definición clase ArtistasEN
namespace QuaverGenNHibernate.EN.P3
{
public partial class ArtistasEN
{
/**
 *	Atributo nombre
 */
private string nombre;



/**
 *	Atributo num_seguidores
 */
private int num_seguidores;



/**
 *	Atributo id
 */
private int id;



/**
 *	Atributo album
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.AlbumesEN> album;



/**
 *	Atributo cancion
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> cancion;






public virtual string Nombre {
        get { return nombre; } set { nombre = value;  }
}



public virtual int Num_seguidores {
        get { return num_seguidores; } set { num_seguidores = value;  }
}



public virtual int Id {
        get { return id; } set { id = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.AlbumesEN> Album {
        get { return album; } set { album = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> Cancion {
        get { return cancion; } set { cancion = value;  }
}





public ArtistasEN()
{
        album = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.AlbumesEN>();
        cancion = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.CancionEN>();
}



public ArtistasEN(int id, string nombre, int num_seguidores, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.AlbumesEN> album, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> cancion
                  )
{
        this.init (Id, nombre, num_seguidores, album, cancion);
}


public ArtistasEN(ArtistasEN artistas)
{
        this.init (Id, artistas.Nombre, artistas.Num_seguidores, artistas.Album, artistas.Cancion);
}

private void init (int id
                   , string nombre, int num_seguidores, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.AlbumesEN> album, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> cancion)
{
        this.Id = id;


        this.Nombre = nombre;

        this.Num_seguidores = num_seguidores;

        this.Album = album;

        this.Cancion = cancion;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        ArtistasEN t = obj as ArtistasEN;
        if (t == null)
                return false;
        if (Id.Equals (t.Id))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Id.GetHashCode ();
        return hash;
}
}
}
