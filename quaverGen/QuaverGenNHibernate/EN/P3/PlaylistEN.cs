
using System;
// Definición clase PlaylistEN
namespace QuaverGenNHibernate.EN.P3
{
public partial class PlaylistEN
{
/**
 *	Atributo nombre
 */
private string nombre;



/**
 *	Atributo descripcion
 */
private string descripcion;



/**
 *	Atributo num_canciones
 */
private string num_canciones;



/**
 *	Atributo fecha_creacion
 */
private Nullable<DateTime> fecha_creacion;



/**
 *	Atributo id
 */
private string id;



/**
 *	Atributo canciones
 */
private System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> canciones;



/**
 *	Atributo tipo_playlist
 */
private QuaverGenNHibernate.Enumerated.P3.Tipo_playlistEnum tipo_playlist;






public virtual string Nombre {
        get { return nombre; } set { nombre = value;  }
}



public virtual string Descripcion {
        get { return descripcion; } set { descripcion = value;  }
}



public virtual string Num_canciones {
        get { return num_canciones; } set { num_canciones = value;  }
}



public virtual Nullable<DateTime> Fecha_creacion {
        get { return fecha_creacion; } set { fecha_creacion = value;  }
}



public virtual string Id {
        get { return id; } set { id = value;  }
}



public virtual System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> Canciones {
        get { return canciones; } set { canciones = value;  }
}



public virtual QuaverGenNHibernate.Enumerated.P3.Tipo_playlistEnum Tipo_playlist {
        get { return tipo_playlist; } set { tipo_playlist = value;  }
}





public PlaylistEN()
{
        canciones = new System.Collections.Generic.List<QuaverGenNHibernate.EN.P3.CancionEN>();
}



public PlaylistEN(string id, string nombre, string descripcion, string num_canciones, Nullable<DateTime> fecha_creacion, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> canciones, QuaverGenNHibernate.Enumerated.P3.Tipo_playlistEnum tipo_playlist
                  )
{
        this.init (Id, nombre, descripcion, num_canciones, fecha_creacion, canciones, tipo_playlist);
}


public PlaylistEN(PlaylistEN playlist)
{
        this.init (Id, playlist.Nombre, playlist.Descripcion, playlist.Num_canciones, playlist.Fecha_creacion, playlist.Canciones, playlist.Tipo_playlist);
}

private void init (string id
                   , string nombre, string descripcion, string num_canciones, Nullable<DateTime> fecha_creacion, System.Collections.Generic.IList<QuaverGenNHibernate.EN.P3.CancionEN> canciones, QuaverGenNHibernate.Enumerated.P3.Tipo_playlistEnum tipo_playlist)
{
        this.Id = id;


        this.Nombre = nombre;

        this.Descripcion = descripcion;

        this.Num_canciones = num_canciones;

        this.Fecha_creacion = fecha_creacion;

        this.Canciones = canciones;

        this.Tipo_playlist = tipo_playlist;
}

public override bool Equals (object obj)
{
        if (obj == null)
                return false;
        PlaylistEN t = obj as PlaylistEN;
        if (t == null)
                return false;
        if (Id.Equals (t.Id))
                return true;
        else
                return false;
}

public override int GetHashCode ()
{
        int hash = 13;

        hash += this.Id.GetHashCode ();
        return hash;
}
}
}
