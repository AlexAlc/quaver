﻿using QuaverGenNHibernate.EN.P3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class AssemblerMusica
    {
         public Musica ConvertENToModelUI(CancionEN en)
        {
            Musica m = new Musica();
            m.titulo = en.Nombre;
            m.genero = en.Genero;
            m.artista = en.Artistas;
            m.valoracion = en.Valoraciones;
           // m.playlist = en.Playlist;
            m.minuto = en.Min_cancion;
            m.letra = en.Letra;

            return m;
        }
        public IList<Musica> ConvertListENToModel(IList<CancionEN> ens)
        {
            IList<Musica> arts = new List<Musica>();
            foreach (CancionEN en in ens)
            {
                arts.Add(ConvertENToModelUI(en));
            }
            return arts;
        }
    }
}