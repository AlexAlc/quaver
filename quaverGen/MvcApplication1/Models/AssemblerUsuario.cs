﻿using QuaverGenNHibernate.EN.P3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class AssemblerUsuario
    {
        public Usuario ConvertENToModelUI(UsuarioEN en)
        {
            Usuario u = new Usuario();
            u.Nombre = en.Nombre;
            u.Apellidos = en.Apellidos;
            u.Nombre_usu = en.Nombre_usuario;
            u.password = en.Contrasenya;
            u.Correo = en.Email;
            u.Fecha = en.Fecha_nac;
            u.TipoUsuario = en.Tipo_usuario;


           
            return u;


        }
        public IList<Usuario> ConvertListENToModel(IList<UsuarioEN> ens)
        {
            IList<Usuario> arts = new List<Usuario>();
            foreach (UsuarioEN en in ens)
            {
                arts.Add(ConvertENToModelUI(en));
            }
            return arts;
        }

    }
}
