﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Usuario
    {
        [ScaffoldColumn(false)]
        public int id { get; set; }

        [ScaffoldColumn(false)]
        public int IdCategoria { get; set; }

        /*    [ScaffoldColumn(false)]
        public IList<Comentario> { get; set; }*/
        //Tambien tengo que crear el model de comentario y el assembler

        /*    [ScaffoldColumn(false)]
      public IList<Entradas> { get; set; }*/

        //Model y Assembler van muy relacionados por que en el assembler asigno bien las variables que creo en el model
        //y todo esto es para los detalles del evento
        [Display(Prompt = "Nombre Usuario", Description = "Nombre del usuario", Name = "Nombre Usuario ")]
        [Required(ErrorMessage = "Debe indicar un nombre para el usuario")]
        [StringLength(maximumLength: 50, ErrorMessage = "El nombre no puede tener más de 50 caracteres")]
        public string Nombre_usu { get; set; }

        [Display(Prompt = "Nombre", Description = "Nombre del usuario", Name = "Nombre ")]
        [Required(ErrorMessage = "Debe indicar un nombre para el usuario")]
        [StringLength(maximumLength: 50, ErrorMessage = "El nombre no puede tener más de 50 caracteres")]
        public string Nombre { get; set; }

        [Display(Prompt = "Apellidos", Description = "Apellidos del usuario", Name = "Apellidos")]
        [Required(ErrorMessage = "Debe indicar un apellido para el usuario")]
        [StringLength(maximumLength: 50, ErrorMessage = "El apellido no puede tener más de 50 caracteres")]
        public string Apellidos { get; set; }

        [Display(Prompt = "Email", Description = "Dirección del usuario", Name = "Direccion")]
        [Required(ErrorMessage = "Debe indicar una dirrección para el usuario")]
        [StringLength(maximumLength: 50, ErrorMessage = "La dirección no puede tener más de 50 caracteres")]
        public string Correo { get; set; }

        [Display(Prompt = "Contraseña", Description = "Contraseña del usuario", Name = "Contraseña")]
        [Required(ErrorMessage = "Debe indicar una contraseña para el usuario")]
        [StringLength(maximumLength: 50, ErrorMessage = "La contraseña no puede tener más de 50 caracteres")]
        public string password { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha Nacimiento")]
        public Nullable<DateTime> Fecha { get; set; }

        [Display(Prompt = "Tipo", Description= "Tipo", Name= "Tipo")]
        [Required(ErrorMessage="Debe indicar un tipo de usuario")]
        public QuaverGenNHibernate.Enumerated.P3.TipoEnum TipoUsuario { get; set; }
        //Faltan Hora, Tipo, Incidencias, Entradas...
    }
 }
