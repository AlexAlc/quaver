﻿using QuaverGenNHibernate.EN.P3;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Musica
    {

        [ScaffoldColumn(false)]
        public int id { get; set; }

        [Display(Prompt = "Titulo canción", Description = "Titulo canción", Name = "Titulo canción")]
        [Required(ErrorMessage = "Debe indicar un nombre para la cancion")]
        [StringLength(maximumLength: 50, ErrorMessage = "La cancion no puede tener más de 50 caracteres")]
        public string titulo { get; set; }

        [Display(Prompt = "Género", Description = "Género canción", Name = "Género ")]
        [Required(ErrorMessage = "Debe un género")]
        [StringLength(maximumLength: 20, ErrorMessage = "El género no puede tener más de 20 caracteres")]
        public string genero { get; set; }

        [Display(Prompt = "Duración", Description = "Duración de la cancion", Name = "Duración")]
        [Required(ErrorMessage = "Debe indicar una duracion para la cancion")]
        public string duracion { get; set; }

        [Display(Prompt = "Usuario", Description = "nombre usuario", Name = "Usuario")]
        [Required(ErrorMessage = "Debe indicar un nombre para el usuario")]
        [StringLength(maximumLength: 50, ErrorMessage = "El nombre no puede tener más de 50 caracteres")]
        public string usuario { get; set; }

        [Display(Prompt = "Album", Description = "Album de la cancion", Name = "Album")]
        [Required(ErrorMessage = "Debe indicar un álbum para la canción")]
        [StringLength(maximumLength: 50, ErrorMessage = "El álbum no puede tener más de 50 caracteres")]
        public string album { get; set; }

        [Display(Prompt = "Artista", Description = "Artista", Name = "Artista")]
        [Required(ErrorMessage = "Debe indicar un artista")]
        public IList<ArtistasEN>artista { get; set; }

        [Display(Prompt = "Valoración", Description = "Valoración de la canción", Name = "Valoración")]
        [Required(ErrorMessage = "Debe indicar una valoración")]
        public ValoracionesEN valoracion { get; set; }

        [Display(Prompt = "Minuto ", Description = "Minuto de la cancion", Name = "Minuto")]
        [Required(ErrorMessage = "Debe indicar un minuto")]
        public Nullable<DateTime> minuto { get; set; }

        [Display(Prompt = "Letra", Description = "Letra", Name = "Letra")]
        [Required(ErrorMessage = "Debe indicar una letra")]
        public string letra { get; set; }
    }
}