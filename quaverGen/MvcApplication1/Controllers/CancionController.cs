﻿using MvcApplication1.Models;
using QuaverGenNHibernate.CAD.P3;
using QuaverGenNHibernate.CEN.P3;
using QuaverGenNHibernate.EN.P3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class CancionController : BasicController
    {
        //
        // GET: /Cancion/

        [HttpPost]
        public ActionResult Anyadir_cancion(CancionEN cancion)
        {
            try
            {
                // TODO: Add insert logic here

                CancionCEN cen = new CancionCEN();
                //necesito un usuario cen

                string auxiliar = cen.New_(cancion.Nombre, cancion.Genero, cancion.Duracion, cancion.R_album, cancion.Artistas, cancion.Min_cancion, cancion.Letra);

                return RedirectToAction("Musica", "Home");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Index()
        {
            CancionCEN caCEN = new CancionCEN();
            //var result = evCEN.get_IEventoCAD();
            IEnumerable<CancionEN> list = caCEN.ReadAll(0, -1).ToList();
            return View(list);
        }

        //
        // GET: /Cancion/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Cancion/Create

        public ActionResult Create()
        {
            Musica cancion = new Musica();
            return View(cancion);
        }

        //
        // POST: /Cancion/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Cancion/Edit/5

        public ActionResult Edit(string id)
        {
            Musica art = null;
            SessionInitialize();
            CancionEN artEN = new CancionCAD(session).ReadOIDDefault(id);
       
            art = new AssemblerMusica().ConvertENToModelUI(artEN);
            SessionClose();
            return View(art);
        }


        //
        // POST: /Cancion/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Cancion/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Cancion/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public NHibernate.ISession session { get; set; }

        public ActionResult Buscar(string palabra)
        {
            IEnumerable<CancionEN> list = null;
            IList<CancionEN> lista = new List<CancionEN>();
            CancionCEN caCEN = new CancionCEN();
            bool ok = false;
            SessionInitialize();
            if (palabra != null)
            {
                list = caCEN.ReadNombre(palabra).ToList();
                if (list == null)
                {
                    ok = true;
                    lista = caCEN.ReadArtista(palabra).ToList();
                    list = lista.ToList();
                }
            }
            SessionClose();

            if (ok == true && lista.Count < 1)
            {
                return RedirectToAction("Buscar", "Musica", routeValues: new { palabra = palabra });
            }
            else
            {
                return View(list);
            }
        }

        public ActionResult Busqueda_avanzada(string nombre, string artista)
        {
            IEnumerable<CancionEN> list = null;
            List<CancionEN> lista = new List<CancionEN>();
            CancionCEN caCEN = new CancionCEN();
            bool ok = false;
            SessionInitialize();
            if (nombre != "")
            {
                list = caCEN.ReadNombre(nombre).ToList();
                if (artista != "")
                {
                    foreach (CancionEN item in list)
                    {
                        if (item.Artistas.ElementAt(0).Nombre == artista)
                        {
                            lista.Add(item);
                            ok = true;
                        }
                    }
                    if (ok)
                        list = lista.ToList();
                }
            }
            if (nombre == "" && artista != "")
            {
                list = caCEN.ReadArtista(artista).ToList();
            }


            SessionClose();
            return View(list);
        }
    }
}
