﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using MvcApplication1.Filters;
using MvcApplication1.Models;
using QuaverGenNHibernate.CEN.P3;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;
using System.IO;


namespace MvcApplication1.Controllers
{
    public class HomeController : BasicController
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Musica()
        {
            CancionCEN canCEN = new CancionCEN();
            IEnumerable<CancionEN> canciones = canCEN.ReadAll(0, -1).ToList();

            return View(canciones);
        }
    }
}
