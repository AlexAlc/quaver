﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using MvcApplication1.Filters;
using MvcApplication1.Models;
using QuaverGenNHibernate.CEN.P3;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CAD.P3;
using System.IO;


namespace MvcApplication1.Controllers
{
    public class UsuarioController : BasicController
    {
        //
        // GET: /Usuario/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Usuario/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Usuario/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Usuario/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Usuario/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Usuario/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Usuario/Delete/5

        public ActionResult Delete(string id)
        {
            SessionInitialize();
            UsuarioCAD usuCAD = new UsuarioCAD(session);

            UsuarioEN usuEN = usuCAD.ReadOID(id);

            new UsuarioCEN().Borrar_cuenta(usuEN.Nombre_usuario);
            SessionClose();
            WebSecurity.Logout();
            return RedirectToAction("../Account/Login");

        }

        //
        // POST: /Usuario/Delete/5

        /*[HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }*/

        public ActionResult Perfil(string name)
        {
            //Usuario usu = null;
            SessionInitialize();
            UsuarioEN usuEN = new UsuarioCAD(session).ReadOIDDefault(name);
            //usu = new AssemblerUsuario().ConvertENToModelUI(usuEN);
            SessionClose();
            return View(usuEN);
        }

        public ActionResult Amigos(string name)
        {

            IEnumerable<UsuarioEN> amigos = null;
            IList<UsuarioEN> todos = new List<UsuarioEN>();
            IList<UsuarioEN> lista = new List<UsuarioEN>();
            bool encontrado = false;
            UsuarioEN usuCEN = new UsuarioEN();

            SessionInitialize();
            todos = new UsuarioCAD(session).ReadAllDefault(0,-1);
            if (todos != null)
            {
                foreach (var item in todos)
                {
                    if (item.Nombre_usuario.Equals(usuCEN.Amigo))
                    {
                        lista.Add(item);
                        encontrado = true;
                    }
                }
                if (encontrado)
                    amigos = lista.ToList();
                // todos = usuCEN.ReadAllDefault(0, -1);
            }
            SessionClose();
            return View(amigos);
        }

        public ActionResult Anyadir_amigo(string id)
        {
            SessionInitialize();

            IEnumerable<UsuarioEN> amigos = null;

            amigos = new UsuarioCAD(session).ReadAllDefault(0, 10).ToList();

            SessionClose();

            return View(amigos);
        }


        public ActionResult Anyadir_amigo_al_usuario(string name)
        {
            SessionInitialize();

            IEnumerable<UsuarioEN> users = null;
            IList<String> amigos = new List<String>();

            users = new UsuarioCAD(session).ReadAllDefault(0, 10).ToList();


            foreach (var item in users)
            {

                if (item.Nombre.Equals(name))
                {

                    amigos.Add(item.Nombre);
                }

            }


            UsuarioCAD usuCAD = new UsuarioCAD(session);

            usuCAD.Agregar_amigo(name, amigos);


            SessionClose();

            return RedirectToAction("Amigos");
        }
    }
}
