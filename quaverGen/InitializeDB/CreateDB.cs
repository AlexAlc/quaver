
/*PROTECTED REGION ID(CreateDB_imports) ENABLED START*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using QuaverGenNHibernate.EN.P3;
using QuaverGenNHibernate.CEN.P3;
using QuaverGenNHibernate.CAD.P3;

/*PROTECTED REGION END*/
namespace InitializeDB
{
public class CreateDB
{
public static void Create (string databaseArg, string userArg, string passArg)
{
        String database = databaseArg;
        String user = userArg;
        String pass = passArg;

        // Conex DB
        SqlConnection cnn = new SqlConnection (@"Server=(local)\sqlexpress; database=master; integrated security=yes");

        // Order T-SQL create user
        String createUser = @"IF NOT EXISTS(SELECT name FROM master.dbo.syslogins WHERE name = '" + user + @"')
            BEGIN
                CREATE LOGIN ["+ user + @"] WITH PASSWORD=N'" + pass + @"', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
            END"                                                                                                                                                                                                                                                                                    ;

        //Order delete user if exist
        String deleteDataBase = @"if exists(select * from sys.databases where name = '" + database + "') DROP DATABASE [" + database + "]";
        //Order create databas
        string createBD = "CREATE DATABASE " + database;
        //Order associate user with database
        String associatedUser = @"USE [" + database + "];CREATE USER [" + user + "] FOR LOGIN [" + user + "];USE [" + database + "];EXEC sp_addrolemember N'db_owner', N'" + user + "'";
        SqlCommand cmd = null;

        try
        {
                // Open conex
                cnn.Open ();

                //Create user in SQLSERVER
                cmd = new SqlCommand (createUser, cnn);
                cmd.ExecuteNonQuery ();

                //DELETE database if exist
                cmd = new SqlCommand (deleteDataBase, cnn);
                cmd.ExecuteNonQuery ();

                //CREATE DB
                cmd = new SqlCommand (createBD, cnn);
                cmd.ExecuteNonQuery ();

                //Associate user with db
                cmd = new SqlCommand (associatedUser, cnn);
                cmd.ExecuteNonQuery ();

                System.Console.WriteLine ("DataBase create sucessfully..");
        }
        catch (Exception ex)
        {
                throw ex;
        }
        finally
        {
                if (cnn.State == ConnectionState.Open) {
                        cnn.Close ();
                }
        }
}

public static void InitializeData()
{
    /*PROTECTED REGION ID(initializeDataMethod) ENABLED START*/
    try
    {



        //USUARIO
        IUsuarioCAD _IusuarioCAD = new UsuarioCAD();
        UsuarioEN usuarioEN = new UsuarioEN();
        UsuarioCEN usuarioCEN = new UsuarioCEN(_IusuarioCAD);

        usuarioEN.Nombre = "Clara";
        usuarioEN.Apellidos = "Saquero";
        usuarioEN.Email = "csr56@alu.ua.es";
        usuarioEN.Fecha_nac = new DateTime(1996, 10, 28);
        usuarioEN.Nombre_usuario = "clarasaq";
        usuarioEN.Contrasenya = "Hola123";
        usuarioEN.Tipo_usuario = QuaverGenNHibernate.Enumerated.P3.TipoEnum.no_premium;

        usuarioCEN.Crear_usuario(usuarioEN.Nombre, usuarioEN.Apellidos, usuarioEN.Nombre_usuario, usuarioEN.Email, usuarioEN.Contrasenya, usuarioEN.Fecha_nac, usuarioEN.Tipo_usuario);




        //USUARIO 2
        IUsuarioCAD _IusuarioCAD1 = new UsuarioCAD();
        UsuarioEN usuarioEN1 = new UsuarioEN();
        UsuarioCEN usuarioCEN1 = new UsuarioCEN(_IusuarioCAD1);

        usuarioEN1.Nombre = "Anabel";
        usuarioEN1.Apellidos = "Diez";
        usuarioEN1.Email = "aidm5@alu.ua.es";
        usuarioEN1.Fecha_nac = new DateTime(1996, 04, 10);
        usuarioEN1.Nombre_usuario = "anabanana";
        usuarioEN1.Contrasenya = "Hola123";
        usuarioEN1.Tipo_usuario = QuaverGenNHibernate.Enumerated.P3.TipoEnum.premium;

        usuarioCEN1.Crear_usuario(usuarioEN1.Nombre, usuarioEN1.Apellidos, usuarioEN1.Nombre_usuario, usuarioEN1.Email, usuarioEN1.Contrasenya, usuarioEN1.Fecha_nac, usuarioEN1.Tipo_usuario);

        //Usuario3
        IUsuarioCAD _IusuarioCAD2 = new UsuarioCAD();
        UsuarioEN usuarioEN2 = new UsuarioEN();
        UsuarioCEN usuarioCEN2 = new UsuarioCEN(_IusuarioCAD2);

        usuarioEN2.Nombre = "Alex";
        usuarioEN2.Apellidos = "Mena";
        usuarioEN2.Email = "amg273@alu.ua.es";
        usuarioEN2.Fecha_nac = new DateTime(1996, 10, 28);
        usuarioEN2.Nombre_usuario = "amguno";
        usuarioEN2.Contrasenya = "Hola123";
        usuarioEN2.Tipo_usuario = QuaverGenNHibernate.Enumerated.P3.TipoEnum.premium;

        usuarioCEN2.Crear_usuario(usuarioEN2.Nombre, usuarioEN2.Apellidos, usuarioEN2.Nombre_usuario, usuarioEN2.Email, usuarioEN2.Contrasenya, usuarioEN2.Fecha_nac, usuarioEN2.Tipo_usuario);
        
        //usuario 4
        IUsuarioCAD _IusuarioCAD3 = new UsuarioCAD();
        UsuarioEN usuarioEN3 = new UsuarioEN();
        UsuarioCEN usuarioCEN3 = new UsuarioCEN(_IusuarioCAD3);

        usuarioEN3.Nombre = "Marta";
        usuarioEN3.Apellidos = "Fuentes";
        usuarioEN3.Email = "mfo54@alu.ua.es";
        usuarioEN3.Fecha_nac = new DateTime(1996, 08, 08);
        usuarioEN3.Nombre_usuario = "marfuor";
        usuarioEN3.Contrasenya = "Hola123";
        usuarioEN3.Tipo_usuario = QuaverGenNHibernate.Enumerated.P3.TipoEnum.no_premium;

        usuarioCEN3.Crear_usuario(usuarioEN3.Nombre, usuarioEN3.Apellidos, usuarioEN3.Nombre_usuario, usuarioEN3.Email, usuarioEN3.Contrasenya, usuarioEN3.Fecha_nac, usuarioEN3.Tipo_usuario);

        // artista
        IArtistasCAD _IArtistasCAD = new ArtistasCAD();
        ArtistasEN artistaEN = new ArtistasEN();
        ArtistasCEN artistaCEN = new ArtistasCEN(_IArtistasCAD);

        artistaEN.Nombre = "Izal";
        artistaEN.Num_seguidores = 250060;
        artistaEN.Id = 1;

        artistaCEN.Crear_artista(artistaEN.Nombre, artistaEN.Num_seguidores, artistaEN.Id);

        //artista 2
        IArtistasCAD _IArtistasCAD2 = new ArtistasCAD();
        ArtistasEN artistaEN2 = new ArtistasEN();
        ArtistasCEN artistaCEN2 = new ArtistasCEN(_IArtistasCAD2);

        artistaEN2.Nombre = "Sia";
        artistaEN2.Num_seguidores = 402360;
        artistaEN2.Id = 2;

        artistaCEN2.Crear_artista(artistaEN2.Nombre, artistaEN2.Num_seguidores, artistaEN2.Id);

        //albumes

        IAlbumesCAD _IalbumesCAD = new AlbumesCAD();
        AlbumesEN albumesEN = new AlbumesEN();
        AlbumesCEN albumesCEN = new AlbumesCEN(_IalbumesCAD);

        albumesEN.Nombre = "Album Copacabana";
        //albumesEN.Cancion = cancion1;
        albumesEN.Numero_canciones = 1;
        albumesEN.Id = 1;

        albumesCEN.Crear_album(albumesEN.Nombre, albumesEN.Id);

        //albumes 2

        IAlbumesCAD _IalbumesCAD2 = new AlbumesCAD();
        AlbumesEN albumesEN2 = new AlbumesEN();
        AlbumesCEN albumesCEN2 = new AlbumesCEN(_IalbumesCAD2);

        albumesEN2.Nombre = "Album Copacabana";
        //albumesEN2.Cancion = cancion1;
        albumesEN2.Numero_canciones = 1;
        albumesEN2.Id = 1;

        albumesCEN.Crear_album(albumesEN2.Nombre, albumesEN2.Id);

        //CANCION
        System.Collections.Generic.List<ArtistasEN> artista1, artista2 = new List<ArtistasEN>(); //lista de artistas

        artista1 = new List<ArtistasEN>();
        artista2 = new List<ArtistasEN>();
        artista1.Add(artistaEN);
        artista2.Add(artistaEN2);

        ICancionCAD _ICancionCAD = new CancionCAD();
        CancionCEN cancionCEN = new CancionCEN(_ICancionCAD);
        CancionEN cancionEN = new CancionEN();
        //AlbumesEN albumesEN = new AlbumesEN();

        cancionEN.Nombre = "Copacabana";
        cancionEN.Genero = "Indie";
        cancionEN.Duracion = new DateTime(2017, 01, 09, 00, 03, 45);
        cancionEN.R_album = albumesEN;
        cancionEN.Artistas = artista1;
        cancionEN.Min_cancion = new DateTime(2017, 01, 09, 00, 03, 45);
        cancionEN.Letra = "Copacabana";

        cancionCEN.New_(cancionEN.Nombre, cancionEN.Genero, cancionEN.Duracion, cancionEN.R_album, cancionEN.Artistas, cancionEN.Min_cancion,cancionEN.Letra);

        //cancion 2
        //AlbumesEN albumesEN2 = new AlbumesEN();

        ICancionCAD _ICancionCAD2 = new CancionCAD();
        CancionCEN cancionCEN2 = new CancionCEN(_ICancionCAD2);
        CancionEN cancionEN2 = new CancionEN();

        cancionEN2.Nombre = "The Gratest";
        cancionEN2.Genero = "Pop";
        cancionEN2.Duracion = new DateTime(2017, 01, 09, 00, 04, 35);
        cancionEN2.R_album = albumesEN2;
        cancionEN2.Artistas = artista2;
        cancionEN2.Min_cancion = new DateTime(2017, 01, 09, 00, 04, 35);
        cancionEN2.Letra = "The greatest";

        cancionCEN2.New_(cancionEN2.Nombre, cancionEN2.Genero, cancionEN2.Duracion, cancionEN2.R_album, cancionEN2.Artistas, cancionEN2.Min_cancion, cancionEN2.Letra);


        

        // lista de usuarios
        System.Collections.Generic.List<String> usuarios1 = new List<string>();
        usuarios1.Add("otro");
        usuarios1.Add("notengo");

       
        System.Collections.Generic.List<CancionEN> cancion1, cancion2 = new List<CancionEN>(); //lista de canciones
        cancion1 = new List<CancionEN>();
        cancion2 = new List<CancionEN>();
        cancion1.Add(cancionEN);
        cancion2.Add(cancionEN2);

        //PLAYLIST

        IPlaylistCAD _IplaylistCAD = new PlaylistCAD();
        PlaylistEN playlistEN = new PlaylistEN();
        PlaylistCEN playlistCEN = new PlaylistCEN(_IplaylistCAD);

        playlistEN.Nombre = "Indie internacional";
        playlistEN.Descripcion = "Las mejores canciones de indie del mundo";
        playlistEN.Num_canciones = "10";
        playlistEN.Fecha_creacion = new DateTime(2017, 01, 07, 00, 00, 00);
        playlistEN.Id = "1";
        playlistEN.Canciones = cancion1;
        playlistEN.Tipo_playlist = QuaverGenNHibernate.Enumerated.P3.Tipo_playlistEnum.privada;

        playlistCEN.Crear_playlist(playlistEN.Nombre, playlistEN.Descripcion,playlistEN.Fecha_creacion,playlistEN.Num_canciones, playlistEN.Tipo_playlist);


       

        // AdMINISTRADOR

        IAdministradorCAD _IadministradorCAD = new AdministradorCAD();
        AdministradorEN administradorEN = new AdministradorEN();
        AdministradorCEN administradorCEN = new AdministradorCEN(_IadministradorCAD);

        administradorEN.Cancion = cancion1;
        administradorEN.Nombre = "anabanana";
        administradorEN.Contrasenya = "Hola123";

        administradorCEN.Crear_admin(administradorEN.Nombre, administradorEN.Contrasenya);

    

        Console.WriteLine("------------------------------");
        Console.WriteLine("------------------------------");
        Console.WriteLine("AQUI TERMINA VA TODO DE PUTA MADRE");
        Console.WriteLine("------------------------------");
        Console.WriteLine("------------------------------");



        
    }
    catch (Exception ex)
    {
        System.Console.WriteLine(ex.InnerException);
        throw ex;
    }
}
}
}
